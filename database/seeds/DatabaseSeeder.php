<?php

use Illuminate\Database\Seeder;

use App\TipoPersona;
use App\RolPersona;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class,1)->create();
        factory(TipoPersona::class,1)->create();
        factory(RolPersona::class,10)->create();
        factory(App\Persona::class,200)->create();
    }
}
