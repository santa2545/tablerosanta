<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendarios', function (Blueprint $table) {
            $table->bigIncrements('id_calendario');
            $table->string('tipo',100);
            $table->string('descripcion',200);
            $table->string('asunto',100);
            $table->unsignedBigInteger('id_juzgado');
            $table->unsignedBigInteger('id_persona');
            $table->unsignedBigInteger('id_nota');
            $table->unsignedBigInteger('id_expediente');
             $table->foreign('id_persona')->references('id_persona')->on('personas')
                ->onUpdate('cascade');
             $table->foreign('id_juzgado')->references('id_juzgado')->on('juzgados')
                ->onUpdate('cascade');
            $table->foreign('id_nota')->references('id_nota')->on('notas')
                ->onUpdate('cascade');
            $table->foreign('id_expediente')->references('id_expediente')->on('expedientes')
                ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendarios');
    }
}
