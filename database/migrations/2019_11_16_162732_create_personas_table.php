<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function (Blueprint $table) {
            $table->bigIncrements('id_persona');
            $table->unsignedBigInteger('idrol');
            $table->string('nombre',100);
            $table->string('documento',16);
            $table->enum('genero',['M','F','OTROS']);
            $table->enum('tipo_docu',['DNI','RUC','OTROS']);
            $table->string('telefono',16)->nullable();
            $table->string('email',100)->nullable();

             $table->foreign('idrol')->references('idRol')->on('rol_personas')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas');
    }
}
