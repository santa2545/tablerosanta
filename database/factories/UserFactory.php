<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use App\TipoPersona;
use App\RolPersona;
use App\Persona;

use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});
$factory->define(TipoPersona::class, function (Faker $faker) {
    return [
        'descripcion' => 'NATURAL',
      ];
});
$factory->define(RolPersona::class, function (Faker $faker) {
    return [
    	'rol'=>$faker->name,
        'idt_persona' => TipoPersona::all()->random()->idt_persona,
      ];
});

$factory->define(Persona::class, function (Faker $faker) {
    return [
    	'idrol'=>RolPersona::all()->random()->idRol,
    	'nombre'=>$faker->name,
    	'documento'=>$faker->numberBetween($min = 1000, $max = 9000).''.$faker->numberBetween($min = 1000, $max = 9000),
    	'genero'=>$faker->randomElement(['M', 'F']),
    	'tipo_docu'=>'DNI',
    	'telefono'=>'999999999',
    	'email'=>$faker->email,
      ];
});
