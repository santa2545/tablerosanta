-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.7.24 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para tablerocontrol
CREATE DATABASE IF NOT EXISTS `tablerocontrol` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `tablerocontrol`;

-- Volcando estructura para tabla tablerocontrol.actividads
CREATE TABLE IF NOT EXISTS `actividads` (
  `id_actividad` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `duracion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descripcion` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idt_actividad` bigint(20) unsigned NOT NULL,
  `id_persona` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_actividad`),
  KEY `actividads_idt_actividad_foreign` (`idt_actividad`),
  KEY `actividads_id_persona_foreign` (`id_persona`),
  CONSTRAINT `actividads_id_persona_foreign` FOREIGN KEY (`id_persona`) REFERENCES `personas` (`id_persona`) ON UPDATE CASCADE,
  CONSTRAINT `actividads_idt_actividad_foreign` FOREIGN KEY (`idt_actividad`) REFERENCES `tipo_actividads` (`idt_actividad`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla tablerocontrol.actividads: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `actividads` DISABLE KEYS */;
INSERT INTO `actividads` (`id_actividad`, `duracion`, `descripcion`, `idt_actividad`, `id_persona`, `created_at`, `updated_at`) VALUES
	(1, '02:30', NULL, 1, 1, '2019-11-17 00:51:41', '2019-11-17 00:51:41'),
	(2, '01:00', NULL, 3, 201, '2019-11-17 01:39:00', '2019-11-17 01:39:00');
/*!40000 ALTER TABLE `actividads` ENABLE KEYS */;

-- Volcando estructura para tabla tablerocontrol.calendarios
CREATE TABLE IF NOT EXISTS `calendarios` (
  `id_calendario` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tipo` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `asunto` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_juzgado` bigint(20) unsigned NOT NULL,
  `id_persona` bigint(20) unsigned NOT NULL,
  `id_nota` bigint(20) unsigned NOT NULL,
  `id_expediente` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_calendario`),
  KEY `calendarios_id_persona_foreign` (`id_persona`),
  KEY `calendarios_id_juzgado_foreign` (`id_juzgado`),
  KEY `calendarios_id_nota_foreign` (`id_nota`),
  KEY `calendarios_id_expediente_foreign` (`id_expediente`),
  CONSTRAINT `calendarios_id_expediente_foreign` FOREIGN KEY (`id_expediente`) REFERENCES `expedientes` (`id_expediente`) ON UPDATE CASCADE,
  CONSTRAINT `calendarios_id_juzgado_foreign` FOREIGN KEY (`id_juzgado`) REFERENCES `juzgados` (`id_juzgado`) ON UPDATE CASCADE,
  CONSTRAINT `calendarios_id_nota_foreign` FOREIGN KEY (`id_nota`) REFERENCES `notas` (`id_nota`) ON UPDATE CASCADE,
  CONSTRAINT `calendarios_id_persona_foreign` FOREIGN KEY (`id_persona`) REFERENCES `personas` (`id_persona`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla tablerocontrol.calendarios: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `calendarios` DISABLE KEYS */;
INSERT INTO `calendarios` (`id_calendario`, `tipo`, `descripcion`, `asunto`, `id_juzgado`, `id_persona`, `id_nota`, `id_expediente`, `created_at`, `updated_at`) VALUES
	(1, 'Tipo', 'Reunion externa', 'Citacion', 1, 1, 1, 1, '2019-11-24 10:28:20', '2019-11-24 10:28:20'),
	(2, 'Tipo', 'Reunion con cliente', 'Citacion', 2, 3, 2, 3, '2019-11-24 10:36:58', '2019-11-24 10:36:58');
/*!40000 ALTER TABLE `calendarios` ENABLE KEYS */;

-- Volcando estructura para tabla tablerocontrol.distritos
CREATE TABLE IF NOT EXISTS `distritos` (
  `id_distrito` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_distrito`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla tablerocontrol.distritos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `distritos` DISABLE KEYS */;
INSERT INTO `distritos` (`id_distrito`, `nombre`, `created_at`, `updated_at`) VALUES
	(1, 'RIMAC', NULL, NULL),
	(2, 'S.J.L', '2019-11-24 09:51:35', '2019-11-24 09:51:35');
/*!40000 ALTER TABLE `distritos` ENABLE KEYS */;

-- Volcando estructura para tabla tablerocontrol.estados
CREATE TABLE IF NOT EXISTS `estados` (
  `id_estado` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla tablerocontrol.estados: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `estados` DISABLE KEYS */;
INSERT INTO `estados` (`id_estado`, `descripcion`, `created_at`, `updated_at`) VALUES
	(1, 'EN PROCESO', NULL, NULL),
	(2, 'EJECUTADO', NULL, NULL);
/*!40000 ALTER TABLE `estados` ENABLE KEYS */;

-- Volcando estructura para tabla tablerocontrol.expedientes
CREATE TABLE IF NOT EXISTS `expedientes` (
  `id_expediente` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nro_exp` int(11) NOT NULL,
  `descripcion` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_persona` bigint(20) unsigned NOT NULL,
  `id_proceso` bigint(20) unsigned NOT NULL,
  `id_estado` bigint(20) unsigned NOT NULL,
  `id_juzgado` bigint(20) unsigned NOT NULL,
  `id_actividad` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_expediente`),
  KEY `expedientes_id_persona_foreign` (`id_persona`),
  KEY `expedientes_id_proceso_foreign` (`id_proceso`),
  KEY `expedientes_id_estado_foreign` (`id_estado`),
  KEY `expedientes_id_juzgado_foreign` (`id_juzgado`),
  KEY `expedientes_id_actividad_foreign` (`id_actividad`),
  CONSTRAINT `expedientes_id_actividad_foreign` FOREIGN KEY (`id_actividad`) REFERENCES `actividads` (`id_actividad`) ON UPDATE CASCADE,
  CONSTRAINT `expedientes_id_estado_foreign` FOREIGN KEY (`id_estado`) REFERENCES `estados` (`id_estado`) ON UPDATE CASCADE,
  CONSTRAINT `expedientes_id_juzgado_foreign` FOREIGN KEY (`id_juzgado`) REFERENCES `juzgados` (`id_juzgado`) ON UPDATE CASCADE,
  CONSTRAINT `expedientes_id_persona_foreign` FOREIGN KEY (`id_persona`) REFERENCES `personas` (`id_persona`) ON UPDATE CASCADE,
  CONSTRAINT `expedientes_id_proceso_foreign` FOREIGN KEY (`id_proceso`) REFERENCES `procesos` (`id_proceso`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla tablerocontrol.expedientes: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `expedientes` DISABLE KEYS */;
INSERT INTO `expedientes` (`id_expediente`, `nro_exp`, `descripcion`, `id_persona`, `id_proceso`, `id_estado`, `id_juzgado`, `id_actividad`, `created_at`, `updated_at`) VALUES
	(1, 1, 'Pleito', 1, 1, 1, 1, 1, '2019-11-24 06:26:53', '2019-11-24 06:26:53'),
	(2, 1, 'Pleito', 1, 1, 1, 1, 1, '2019-11-24 06:27:45', '2019-11-24 06:27:45'),
	(3, 3, 'Observacion', 1, 1, 1, 1, 1, '2019-11-24 06:40:25', '2019-11-24 06:40:25');
/*!40000 ALTER TABLE `expedientes` ENABLE KEYS */;

-- Volcando estructura para tabla tablerocontrol.juzgados
CREATE TABLE IF NOT EXISTS `juzgados` (
  `id_juzgado` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nombJuzgado` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_distrito` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_juzgado`),
  KEY `juzgados_id_distrito_foreign` (`id_distrito`),
  CONSTRAINT `juzgados_id_distrito_foreign` FOREIGN KEY (`id_distrito`) REFERENCES `distritos` (`id_distrito`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla tablerocontrol.juzgados: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `juzgados` DISABLE KEYS */;
INSERT INTO `juzgados` (`id_juzgado`, `nombJuzgado`, `id_distrito`, `created_at`, `updated_at`) VALUES
	(1, 'TCP', 1, NULL, NULL),
	(2, 'Paz Letrado', 1, '2019-11-24 09:29:38', '2019-11-24 09:29:38'),
	(3, 'Juzgado N', 1, '2019-11-24 09:29:50', '2019-11-24 09:29:50');
/*!40000 ALTER TABLE `juzgados` ENABLE KEYS */;

-- Volcando estructura para tabla tablerocontrol.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla tablerocontrol.migrations: ~15 rows (aproximadamente)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_11_16_161804_create_tipo_personas_table', 1),
	(4, '2019_11_16_162256_create_rol_personas_table', 1),
	(5, '2019_11_16_162732_create_personas_table', 1),
	(6, '2019_11_16_163425_create_tipo_procesos_table', 1),
	(7, '2019_11_16_163552_create_procesos_table', 1),
	(8, '2019_11_16_163819_create_distritos_table', 1),
	(9, '2019_11_16_164106_create_juzgados_table', 1),
	(10, '2019_11_16_164342_create_estados_table', 1),
	(11, '2019_11_16_164626_create_tipo_actividads_table', 1),
	(12, '2019_11_16_165035_create_actividads_table', 1),
	(13, '2019_11_16_165811_create_expedientes_table', 1),
	(14, '2019_11_16_171509_create_notas_table', 1),
	(15, '2019_11_16_171531_create_calendarios_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Volcando estructura para tabla tablerocontrol.notas
CREATE TABLE IF NOT EXISTS `notas` (
  `id_nota` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_nota`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla tablerocontrol.notas: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `notas` DISABLE KEYS */;
INSERT INTO `notas` (`id_nota`, `descripcion`, `created_at`, `updated_at`) VALUES
	(1, 'Observacion n', NULL, NULL),
	(2, 'Nota 2', '2019-11-24 10:09:45', '2019-11-24 10:09:45');
/*!40000 ALTER TABLE `notas` ENABLE KEYS */;

-- Volcando estructura para tabla tablerocontrol.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla tablerocontrol.password_resets: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Volcando estructura para tabla tablerocontrol.personas
CREATE TABLE IF NOT EXISTS `personas` (
  `id_persona` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idrol` bigint(20) unsigned NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `documento` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `genero` enum('M','F','OTROS') COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_docu` enum('DNI','RUC','OTROS') COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_persona`),
  KEY `personas_idrol_foreign` (`idrol`),
  CONSTRAINT `personas_idrol_foreign` FOREIGN KEY (`idrol`) REFERENCES `rol_personas` (`idRol`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=203 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla tablerocontrol.personas: ~200 rows (aproximadamente)
/*!40000 ALTER TABLE `personas` DISABLE KEYS */;
INSERT INTO `personas` (`id_persona`, `idrol`, `nombre`, `documento`, `genero`, `tipo_docu`, `telefono`, `email`, `created_at`, `updated_at`) VALUES
	(1, 6, 'Sylvia Stroman', '18268313', 'M', 'DNI', '999999999', 'eankunding@hotmail.com', '2019-11-17 00:46:06', '2019-11-17 00:46:06'),
	(2, 5, 'Baby Zieme', '63188535', 'F', 'DNI', '999999999', 'kenton40@rowe.biz', '2019-11-17 00:46:06', '2019-11-17 00:46:06'),
	(3, 5, 'Mr. Oren Paucek I', '85546392', 'F', 'DNI', '999999999', 'willie.jacobi@gmail.com', '2019-11-17 00:46:07', '2019-11-17 00:46:07'),
	(4, 1, 'Zita Orn', '33242945', 'M', 'DNI', '999999999', 'mitchel43@gmail.com', '2019-11-17 00:46:07', '2019-11-17 00:46:07'),
	(5, 2, 'Dwight Haag', '72425022', 'M', 'DNI', '999999999', 'mueller.dock@pollich.com', '2019-11-17 00:46:07', '2019-11-17 00:46:07'),
	(6, 8, 'Mr. Graham Orn I', '35232077', 'M', 'DNI', '999999999', 'kking@mosciski.biz', '2019-11-17 00:46:07', '2019-11-17 00:46:07'),
	(7, 6, 'Miss Camille Walter', '47967874', 'F', 'DNI', '999999999', 'gframi@gmail.com', '2019-11-17 00:46:07', '2019-11-17 00:46:07'),
	(8, 4, 'Prof. Agnes Ondricka III', '35831163', 'F', 'DNI', '999999999', 'darion83@mueller.com', '2019-11-17 00:46:07', '2019-11-17 00:46:07'),
	(9, 8, 'Neal Feest', '23807612', 'M', 'DNI', '999999999', 'norris87@ernser.com', '2019-11-17 00:46:07', '2019-11-17 00:46:07'),
	(10, 9, 'Esta Runte', '49802609', 'M', 'DNI', '999999999', 'adriel66@gmail.com', '2019-11-17 00:46:07', '2019-11-17 00:46:07'),
	(11, 3, 'Lambert Osinski', '34578149', 'M', 'DNI', '999999999', 'abbott.matilda@yahoo.com', '2019-11-17 00:46:07', '2019-11-17 00:46:07'),
	(12, 3, 'Eulalia Buckridge Jr.', '66238028', 'M', 'DNI', '999999999', 'carlie91@bogisich.info', '2019-11-17 00:46:07', '2019-11-17 00:46:07'),
	(13, 1, 'Elliot Bosco', '83772259', 'F', 'DNI', '999999999', 'pearline.sipes@schmidt.net', '2019-11-17 00:46:07', '2019-11-17 00:46:07'),
	(14, 7, 'Asha Cremin', '41056586', 'M', 'DNI', '999999999', 'wolff.chloe@yahoo.com', '2019-11-17 00:46:07', '2019-11-17 00:46:07'),
	(15, 3, 'Elva Swift Jr.', '74662114', 'F', 'DNI', '999999999', 'helene.fisher@brekke.com', '2019-11-17 00:46:07', '2019-11-17 00:46:07'),
	(16, 2, 'Ali Funk', '50356467', 'F', 'DNI', '999999999', 'ggulgowski@gmail.com', '2019-11-17 00:46:07', '2019-11-17 00:46:07'),
	(17, 7, 'Willie Buckridge', '86514321', 'M', 'DNI', '999999999', 'mharvey@gmail.com', '2019-11-17 00:46:07', '2019-11-17 00:46:07'),
	(18, 4, 'Max Maggio Sr.', '89776404', 'F', 'DNI', '999999999', 'missouri.funk@ohara.net', '2019-11-17 00:46:07', '2019-11-17 00:46:07'),
	(19, 2, 'Dr. Jadyn Quitzon Jr.', '63691794', 'M', 'DNI', '999999999', 'marjory.skiles@hotmail.com', '2019-11-17 00:46:07', '2019-11-17 00:46:07'),
	(20, 1, 'Dr. Liza Thompson', '87857685', 'M', 'DNI', '999999999', 'dach.cindy@yahoo.com', '2019-11-17 00:46:08', '2019-11-17 00:46:08'),
	(21, 1, 'Linnie Rowe', '88483243', 'M', 'DNI', '999999999', 'ivolkman@hotmail.com', '2019-11-17 00:46:08', '2019-11-17 00:46:08'),
	(22, 10, 'Ms. Rubye Kuhlman MD', '70988799', 'M', 'DNI', '999999999', 'keyshawn.mcclure@hotmail.com', '2019-11-17 00:46:08', '2019-11-17 00:46:08'),
	(23, 7, 'Abbie Hyatt', '79766574', 'F', 'DNI', '999999999', 'ettie.koelpin@weber.com', '2019-11-17 00:46:08', '2019-11-17 00:46:08'),
	(24, 5, 'Jeff Gottlieb', '80352591', 'F', 'DNI', '999999999', 'maia.cartwright@paucek.com', '2019-11-17 00:46:08', '2019-11-17 00:46:08'),
	(25, 2, 'Sabina Ratke', '62436351', 'M', 'DNI', '999999999', 'pansy.swift@rath.org', '2019-11-17 00:46:08', '2019-11-17 00:46:08'),
	(26, 10, 'Ova Kris', '55106595', 'F', 'DNI', '999999999', 'wyman.bell@hudson.info', '2019-11-17 00:46:08', '2019-11-17 00:46:08'),
	(27, 4, 'Keanu Greenholt', '85567771', 'M', 'DNI', '999999999', 'jfeil@gmail.com', '2019-11-17 00:46:08', '2019-11-17 00:46:08'),
	(28, 9, 'Bernardo Graham', '30028628', 'F', 'DNI', '999999999', 'louisa72@kshlerin.com', '2019-11-17 00:46:08', '2019-11-17 00:46:08'),
	(29, 6, 'Jed Schmidt', '58776063', 'M', 'DNI', '999999999', 'marina26@walter.org', '2019-11-17 00:46:08', '2019-11-17 00:46:08'),
	(30, 1, 'Lowell Skiles', '23703668', 'M', 'DNI', '999999999', 'haag.earlene@moen.com', '2019-11-17 00:46:08', '2019-11-17 00:46:08'),
	(31, 6, 'Ambrose McKenzie', '65885855', 'M', 'DNI', '999999999', 'ashields@cronin.com', '2019-11-17 00:46:08', '2019-11-17 00:46:08'),
	(32, 5, 'Naomie Kozey PhD', '55175097', 'M', 'DNI', '999999999', 'mikayla31@glover.com', '2019-11-17 00:46:08', '2019-11-17 00:46:08'),
	(33, 8, 'Dr. Jakob Prosacco', '41827415', 'F', 'DNI', '999999999', 'eula.legros@thompson.info', '2019-11-17 00:46:08', '2019-11-17 00:46:08'),
	(34, 4, 'Ms. Josephine Feest', '87965641', 'F', 'DNI', '999999999', 'freda.howe@fritsch.com', '2019-11-17 00:46:08', '2019-11-17 00:46:08'),
	(35, 1, 'Antonio Halvorson V', '71283531', 'F', 'DNI', '999999999', 'aracely.jones@wyman.org', '2019-11-17 00:46:08', '2019-11-17 00:46:08'),
	(36, 1, 'Laurianne Willms', '26514746', 'F', 'DNI', '999999999', 'elinore.zulauf@yahoo.com', '2019-11-17 00:46:08', '2019-11-17 00:46:08'),
	(37, 1, 'Jazmin Ratke', '48343452', 'F', 'DNI', '999999999', 'lgorczany@hotmail.com', '2019-11-17 00:46:08', '2019-11-17 00:46:08'),
	(38, 6, 'Prof. Ethel Nienow Sr.', '31536884', 'F', 'DNI', '999999999', 'wwintheiser@gmail.com', '2019-11-17 00:46:08', '2019-11-17 00:46:08'),
	(39, 6, 'Isabell Ryan DDS', '29767179', 'M', 'DNI', '999999999', 'zechariah.okon@hotmail.com', '2019-11-17 00:46:08', '2019-11-17 00:46:08'),
	(40, 10, 'Piper Rosenbaum', '81593504', 'F', 'DNI', '999999999', 'jchamplin@jerde.com', '2019-11-17 00:46:08', '2019-11-17 00:46:08'),
	(41, 7, 'Dr. Jolie Monahan', '49575882', 'F', 'DNI', '999999999', 'angelina.lindgren@konopelski.com', '2019-11-17 00:46:09', '2019-11-17 00:46:09'),
	(42, 9, 'Rashad Hansen', '84762127', 'M', 'DNI', '999999999', 'conn.percy@yahoo.com', '2019-11-17 00:46:09', '2019-11-17 00:46:09'),
	(43, 2, 'Griffin Hoeger', '23247306', 'M', 'DNI', '999999999', 'hettie23@labadie.com', '2019-11-17 00:46:09', '2019-11-17 00:46:09'),
	(44, 4, 'Mrs. Bryana Keebler', '21764086', 'M', 'DNI', '999999999', 'albin80@johnston.biz', '2019-11-17 00:46:09', '2019-11-17 00:46:09'),
	(45, 8, 'Mr. Abdiel Osinski', '60135732', 'F', 'DNI', '999999999', 'gaylord49@veum.com', '2019-11-17 00:46:09', '2019-11-17 00:46:09'),
	(46, 4, 'Brody Doyle', '56822632', 'F', 'DNI', '999999999', 'kilback.esmeralda@murphy.com', '2019-11-17 00:46:09', '2019-11-17 00:46:09'),
	(47, 7, 'Vanessa Feil Jr.', '66306780', 'F', 'DNI', '999999999', 'kennedy.moen@hotmail.com', '2019-11-17 00:46:09', '2019-11-17 00:46:09'),
	(48, 2, 'Troy Towne IV', '35645356', 'F', 'DNI', '999999999', 'meagan46@yahoo.com', '2019-11-17 00:46:09', '2019-11-17 00:46:09'),
	(49, 5, 'Reginald Gorczany', '59074364', 'M', 'DNI', '999999999', 'estevan99@gmail.com', '2019-11-17 00:46:09', '2019-11-17 00:46:09'),
	(50, 7, 'Karine Little', '37837581', 'M', 'DNI', '999999999', 'terrell46@mraz.com', '2019-11-17 00:46:09', '2019-11-17 00:46:09'),
	(51, 5, 'Mr. Kieran Nikolaus I', '61748130', 'F', 'DNI', '999999999', 'cronin.david@yahoo.com', '2019-11-17 00:46:09', '2019-11-17 00:46:09'),
	(52, 8, 'Lura Rohan', '28445433', 'F', 'DNI', '999999999', 'britney.bartoletti@treutel.info', '2019-11-17 00:46:09', '2019-11-17 00:46:09'),
	(53, 3, 'Tyrese Zboncak MD', '53103008', 'M', 'DNI', '999999999', 'carmella.crist@murphy.com', '2019-11-17 00:46:09', '2019-11-17 00:46:09'),
	(54, 8, 'Mr. Alexzander Barrows', '35417797', 'M', 'DNI', '999999999', 'reichel.cullen@kovacek.com', '2019-11-17 00:46:09', '2019-11-17 00:46:09'),
	(55, 7, 'Leilani Kihn', '54028207', 'M', 'DNI', '999999999', 'parker.arnoldo@yundt.com', '2019-11-17 00:46:09', '2019-11-17 00:46:09'),
	(56, 1, 'Merlin Goodwin', '83495966', 'M', 'DNI', '999999999', 'hilpert.jenifer@dare.org', '2019-11-17 00:46:09', '2019-11-17 00:46:09'),
	(57, 2, 'Assunta King', '10492816', 'F', 'DNI', '999999999', 'kulas.landen@watsica.com', '2019-11-17 00:46:09', '2019-11-17 00:46:09'),
	(58, 5, 'Fay Blick', '22624244', 'M', 'DNI', '999999999', 'ulemke@yahoo.com', '2019-11-17 00:46:09', '2019-11-17 00:46:09'),
	(59, 2, 'Mr. Rudolph Labadie MD', '15495996', 'M', 'DNI', '999999999', 'lisette.heathcote@yahoo.com', '2019-11-17 00:46:09', '2019-11-17 00:46:09'),
	(60, 9, 'Prof. Miguel Osinski DVM', '31488411', 'M', 'DNI', '999999999', 'abshire.ransom@yahoo.com', '2019-11-17 00:46:09', '2019-11-17 00:46:09'),
	(61, 8, 'Alycia Sipes', '19166660', 'M', 'DNI', '999999999', 'georgiana15@yahoo.com', '2019-11-17 00:46:09', '2019-11-17 00:46:09'),
	(62, 7, 'Dr. Stephon Satterfield', '20094211', 'F', 'DNI', '999999999', 'toby.bashirian@gmail.com', '2019-11-17 00:46:09', '2019-11-17 00:46:09'),
	(63, 9, 'Lucious Mohr', '75827084', 'M', 'DNI', '999999999', 'brooklyn49@franecki.com', '2019-11-17 00:46:09', '2019-11-17 00:46:09'),
	(64, 5, 'Miss Kassandra Howell MD', '12368119', 'F', 'DNI', '999999999', 'bboehm@gmail.com', '2019-11-17 00:46:10', '2019-11-17 00:46:10'),
	(65, 7, 'Stephen Stokes', '48886266', 'M', 'DNI', '999999999', 'graham.agustin@hotmail.com', '2019-11-17 00:46:10', '2019-11-17 00:46:10'),
	(66, 4, 'Adrian Reinger', '29846810', 'F', 'DNI', '999999999', 'runte.trycia@hotmail.com', '2019-11-17 00:46:10', '2019-11-17 00:46:10'),
	(67, 7, 'Prof. Ian Lockman', '68367881', 'F', 'DNI', '999999999', 'cristobal75@mante.com', '2019-11-17 00:46:10', '2019-11-17 00:46:10'),
	(68, 3, 'Colby Langosh Sr.', '27714734', 'M', 'DNI', '999999999', 'columbus.dickens@wunsch.info', '2019-11-17 00:46:10', '2019-11-17 00:46:10'),
	(69, 6, 'Zackary Bergstrom', '16282250', 'F', 'DNI', '999999999', 'howe.alexis@gmail.com', '2019-11-17 00:46:10', '2019-11-17 00:46:10'),
	(70, 3, 'Dayna Murazik I', '13316145', 'F', 'DNI', '999999999', 'christelle.koepp@gmail.com', '2019-11-17 00:46:10', '2019-11-17 00:46:10'),
	(71, 8, 'Ms. Estrella Conn', '18196758', 'M', 'DNI', '999999999', 'rogahn.leanna@kuhn.com', '2019-11-17 00:46:10', '2019-11-17 00:46:10'),
	(72, 6, 'Cynthia Cole', '62288555', 'M', 'DNI', '999999999', 'leanna.schowalter@yahoo.com', '2019-11-17 00:46:10', '2019-11-17 00:46:10'),
	(73, 4, 'Marian Mosciski', '81275477', 'M', 'DNI', '999999999', 'krystal.ryan@hotmail.com', '2019-11-17 00:46:10', '2019-11-17 00:46:10'),
	(74, 8, 'Humberto Hartmann', '81536666', 'M', 'DNI', '999999999', 'maggie20@reinger.org', '2019-11-17 00:46:10', '2019-11-17 00:46:10'),
	(75, 1, 'Broderick Robel', '42421301', 'F', 'DNI', '999999999', 'cronin.keira@langosh.info', '2019-11-17 00:46:10', '2019-11-17 00:46:10'),
	(76, 5, 'Rachael Kemmer', '31785647', 'F', 'DNI', '999999999', 'heidenreich.xzavier@yahoo.com', '2019-11-17 00:46:10', '2019-11-17 00:46:10'),
	(77, 5, 'Desmond Conroy', '59258770', 'M', 'DNI', '999999999', 'eichmann.clovis@yahoo.com', '2019-11-17 00:46:10', '2019-11-17 00:46:10'),
	(78, 9, 'Trevion Zieme', '85621497', 'F', 'DNI', '999999999', 'sipes.adolf@schultz.info', '2019-11-17 00:46:10', '2019-11-17 00:46:10'),
	(79, 8, 'Garrick Blanda', '16133765', 'M', 'DNI', '999999999', 'qweimann@wunsch.org', '2019-11-17 00:46:10', '2019-11-17 00:46:10'),
	(80, 5, 'Wyatt Cassin', '74982588', 'F', 'DNI', '999999999', 'cummerata.desmond@gmail.com', '2019-11-17 00:46:10', '2019-11-17 00:46:10'),
	(81, 2, 'Anahi Renner', '14101349', 'M', 'DNI', '999999999', 'mraz.elnora@dickinson.com', '2019-11-17 00:46:10', '2019-11-17 00:46:10'),
	(82, 5, 'Faustino McKenzie', '54157433', 'F', 'DNI', '999999999', 'ccremin@hotmail.com', '2019-11-17 00:46:10', '2019-11-17 00:46:10'),
	(83, 3, 'Giuseppe Johnston', '80922502', 'F', 'DNI', '999999999', 'napoleon50@hotmail.com', '2019-11-17 00:46:10', '2019-11-17 00:46:10'),
	(84, 5, 'Rachelle Kuhic', '20348717', 'M', 'DNI', '999999999', 'kmonahan@gibson.biz', '2019-11-17 00:46:10', '2019-11-17 00:46:10'),
	(85, 8, 'Macie Veum', '46854310', 'M', 'DNI', '999999999', 'mills.marlee@yahoo.com', '2019-11-17 00:46:11', '2019-11-17 00:46:11'),
	(86, 3, 'Kevin Koelpin', '29932901', 'M', 'DNI', '999999999', 'hermiston.anya@hotmail.com', '2019-11-17 00:46:11', '2019-11-17 00:46:11'),
	(87, 9, 'Mariela Schultz', '49795641', 'F', 'DNI', '999999999', 'elvera10@hotmail.com', '2019-11-17 00:46:11', '2019-11-17 00:46:11'),
	(88, 5, 'Gabrielle Little', '38342736', 'M', 'DNI', '999999999', 'cyrus.rempel@gmail.com', '2019-11-17 00:46:11', '2019-11-17 00:46:11'),
	(89, 10, 'Leonor Little', '58741027', 'M', 'DNI', '999999999', 'schimmel.halle@gmail.com', '2019-11-17 00:46:11', '2019-11-17 00:46:11'),
	(90, 3, 'Ms. Hassie Breitenberg', '36982537', 'M', 'DNI', '999999999', 'harris.breana@upton.com', '2019-11-17 00:46:11', '2019-11-17 00:46:11'),
	(91, 7, 'Luisa McKenzie', '75162924', 'M', 'DNI', '999999999', 'zwillms@muller.info', '2019-11-17 00:46:11', '2019-11-17 00:46:11'),
	(92, 5, 'Dr. Elva Yundt DVM', '49234850', 'M', 'DNI', '999999999', 'mking@hotmail.com', '2019-11-17 00:46:11', '2019-11-17 00:46:11'),
	(93, 3, 'Minerva White III', '24054143', 'F', 'DNI', '999999999', 'hharris@ohara.com', '2019-11-17 00:46:11', '2019-11-17 00:46:11'),
	(94, 7, 'Carleton Koepp', '30478288', 'F', 'DNI', '999999999', 'bins.wilton@price.net', '2019-11-17 00:46:11', '2019-11-17 00:46:11'),
	(95, 2, 'Elena Greenholt', '89368477', 'M', 'DNI', '999999999', 'norberto.marvin@yahoo.com', '2019-11-17 00:46:11', '2019-11-17 00:46:11'),
	(96, 2, 'Gavin Turcotte PhD', '61373217', 'F', 'DNI', '999999999', 'hleuschke@hirthe.com', '2019-11-17 00:46:11', '2019-11-17 00:46:11'),
	(97, 4, 'Dr. Chauncey Schamberger', '48803487', 'F', 'DNI', '999999999', 'lgreenfelder@parker.org', '2019-11-17 00:46:11', '2019-11-17 00:46:11'),
	(98, 5, 'Jefferey Gislason', '10543311', 'F', 'DNI', '999999999', 'xkerluke@hotmail.com', '2019-11-17 00:46:11', '2019-11-17 00:46:11'),
	(99, 5, 'Irwin Schamberger', '70755317', 'M', 'DNI', '999999999', 'sauer.vern@gmail.com', '2019-11-17 00:46:11', '2019-11-17 00:46:11'),
	(100, 8, 'Nikolas Pagac', '67116375', 'M', 'DNI', '999999999', 'reta.turner@hotmail.com', '2019-11-17 00:46:11', '2019-11-17 00:46:11'),
	(101, 6, 'Prof. Bettye Krajcik II', '10591746', 'M', 'DNI', '999999999', 'simeon.gleichner@yahoo.com', '2019-11-17 00:46:11', '2019-11-17 00:46:11'),
	(102, 9, 'Consuelo Hayes', '19517604', 'F', 'DNI', '999999999', 'yweissnat@hotmail.com', '2019-11-17 00:46:11', '2019-11-17 00:46:11'),
	(103, 5, 'Zoe Quitzon', '75296122', 'M', 'DNI', '999999999', 'gabrielle16@zemlak.com', '2019-11-17 00:46:11', '2019-11-17 00:46:11'),
	(104, 4, 'Prof. Wiley White IV', '38045876', 'F', 'DNI', '999999999', 'kohler.coralie@bednar.com', '2019-11-17 00:46:11', '2019-11-17 00:46:11'),
	(105, 8, 'Germaine Simonis V', '34234156', 'M', 'DNI', '999999999', 'agustina90@brekke.com', '2019-11-17 00:46:11', '2019-11-17 00:46:11'),
	(106, 1, 'Dr. Teagan Torphy', '25076448', 'F', 'DNI', '999999999', 'bechtelar.yazmin@hotmail.com', '2019-11-17 00:46:12', '2019-11-17 00:46:12'),
	(107, 4, 'Bradford Little', '26861481', 'F', 'DNI', '999999999', 'brooklyn.homenick@gmail.com', '2019-11-17 00:46:12', '2019-11-17 00:46:12'),
	(108, 8, 'Anita Rogahn DDS', '58785602', 'M', 'DNI', '999999999', 'isai63@hotmail.com', '2019-11-17 00:46:12', '2019-11-17 00:46:12'),
	(109, 10, 'Brittany Collier', '80337699', 'F', 'DNI', '999999999', 'ohauck@yahoo.com', '2019-11-17 00:46:12', '2019-11-17 00:46:12'),
	(110, 6, 'Prof. Vance Boehm V', '67264503', 'F', 'DNI', '999999999', 'cyrus.schmidt@hotmail.com', '2019-11-17 00:46:12', '2019-11-17 00:46:12'),
	(111, 7, 'Ludwig Little PhD', '61622806', 'F', 'DNI', '999999999', 'armstrong.brady@jacobi.com', '2019-11-17 00:46:12', '2019-11-17 00:46:12'),
	(112, 4, 'Prof. Macey Funk V', '88512143', 'M', 'DNI', '999999999', 'cborer@hotmail.com', '2019-11-17 00:46:12', '2019-11-17 00:46:12'),
	(113, 7, 'Wava Hyatt Jr.', '58281973', 'M', 'DNI', '999999999', 'alessandra41@gmail.com', '2019-11-17 00:46:12', '2019-11-17 00:46:12'),
	(114, 10, 'Mack Hyatt', '25053366', 'F', 'DNI', '999999999', 'qupton@hotmail.com', '2019-11-17 00:46:12', '2019-11-17 00:46:12'),
	(115, 10, 'Pascale Lemke III', '62792801', 'M', 'DNI', '999999999', 'uhuel@goodwin.com', '2019-11-17 00:46:12', '2019-11-17 00:46:12'),
	(116, 1, 'Harmon Cormier', '80844553', 'M', 'DNI', '999999999', 'jkozey@hotmail.com', '2019-11-17 00:46:12', '2019-11-17 00:46:12'),
	(117, 1, 'Dr. Ena Barton DDS', '82926158', 'F', 'DNI', '999999999', 'bartell.terence@cummings.com', '2019-11-17 00:46:12', '2019-11-17 00:46:12'),
	(118, 3, 'Tracy Lynch', '46541876', 'F', 'DNI', '999999999', 'reta.stracke@yahoo.com', '2019-11-17 00:46:12', '2019-11-17 00:46:12'),
	(119, 10, 'Ahmed Dickens', '41006240', 'F', 'DNI', '999999999', 'bernier.conrad@keeling.com', '2019-11-17 00:46:12', '2019-11-17 00:46:12'),
	(120, 8, 'Camryn Keeling', '54923727', 'F', 'DNI', '999999999', 'nicolas.zachary@yahoo.com', '2019-11-17 00:46:12', '2019-11-17 00:46:12'),
	(121, 9, 'Kenny Emmerich', '80881691', 'M', 'DNI', '999999999', 'tony.moen@wiegand.com', '2019-11-17 00:46:12', '2019-11-17 00:46:12'),
	(122, 7, 'Jayda Hessel', '87768768', 'F', 'DNI', '999999999', 'dsporer@hotmail.com', '2019-11-17 00:46:12', '2019-11-17 00:46:12'),
	(123, 6, 'Lindsey Price', '38772896', 'M', 'DNI', '999999999', 'hrunolfsson@bednar.net', '2019-11-17 00:46:12', '2019-11-17 00:46:12'),
	(124, 4, 'Minnie King', '52065243', 'F', 'DNI', '999999999', 'afton71@gmail.com', '2019-11-17 00:46:12', '2019-11-17 00:46:12'),
	(125, 7, 'Eusebio Murazik', '70585345', 'F', 'DNI', '999999999', 'batz.tina@gmail.com', '2019-11-17 00:46:12', '2019-11-17 00:46:12'),
	(126, 7, 'Dolores Bogisich', '70877953', 'F', 'DNI', '999999999', 'madeline.grant@thompson.biz', '2019-11-17 00:46:12', '2019-11-17 00:46:12'),
	(127, 2, 'Olaf Nolan', '60907029', 'M', 'DNI', '999999999', 'chance06@gmail.com', '2019-11-17 00:46:12', '2019-11-17 00:46:12'),
	(128, 3, 'Prof. Lesly Sipes', '52723234', 'F', 'DNI', '999999999', 'qweissnat@romaguera.com', '2019-11-17 00:46:13', '2019-11-17 00:46:13'),
	(129, 8, 'Mr. Archibald Keebler', '45384194', 'M', 'DNI', '999999999', 'goodwin.jason@reilly.net', '2019-11-17 00:46:13', '2019-11-17 00:46:13'),
	(130, 6, 'Elena Towne', '62275982', 'F', 'DNI', '999999999', 'marie.blanda@gmail.com', '2019-11-17 00:46:13', '2019-11-17 00:46:13'),
	(131, 1, 'Kiley Quitzon', '22574019', 'M', 'DNI', '999999999', 'vandervort.junius@olson.com', '2019-11-17 00:46:13', '2019-11-17 00:46:13'),
	(132, 8, 'Leora Gleason', '58171217', 'M', 'DNI', '999999999', 'delpha42@kassulke.com', '2019-11-17 00:46:13', '2019-11-17 00:46:13'),
	(133, 5, 'Bethel Fay', '67244664', 'M', 'DNI', '999999999', 'cgerhold@doyle.com', '2019-11-17 00:46:13', '2019-11-17 00:46:13'),
	(134, 4, 'Gregory Ortiz', '75534248', 'M', 'DNI', '999999999', 'pfeffer.kaitlin@cole.com', '2019-11-17 00:46:13', '2019-11-17 00:46:13'),
	(135, 2, 'Susan Grady', '22132441', 'M', 'DNI', '999999999', 'mollie.jones@kshlerin.com', '2019-11-17 00:46:13', '2019-11-17 00:46:13'),
	(136, 2, 'Dr. Anais Schmitt III', '42172827', 'F', 'DNI', '999999999', 'violet56@hotmail.com', '2019-11-17 00:46:13', '2019-11-17 00:46:13'),
	(137, 7, 'Amber Jacobs', '25932549', 'M', 'DNI', '999999999', 'ksawayn@borer.com', '2019-11-17 00:46:13', '2019-11-17 00:46:13'),
	(138, 2, 'Dario Connelly', '17635853', 'M', 'DNI', '999999999', 'ansley.kuhlman@ward.org', '2019-11-17 00:46:13', '2019-11-17 00:46:13'),
	(139, 9, 'Ryan Harris', '20143875', 'F', 'DNI', '999999999', 'vkuvalis@hotmail.com', '2019-11-17 00:46:13', '2019-11-17 00:46:13'),
	(140, 7, 'Prof. Narciso Mohr', '63777203', 'F', 'DNI', '999999999', 'lhettinger@hoeger.info', '2019-11-17 00:46:13', '2019-11-17 00:46:13'),
	(141, 2, 'Elliot Abernathy', '10311605', 'M', 'DNI', '999999999', 'otto58@rippin.net', '2019-11-17 00:46:13', '2019-11-17 00:46:13'),
	(142, 5, 'Kaleb Mitchell', '48967516', 'M', 'DNI', '999999999', 'domenica.sanford@gmail.com', '2019-11-17 00:46:13', '2019-11-17 00:46:13'),
	(143, 1, 'Santino Stark', '11311335', 'M', 'DNI', '999999999', 'jarrod.cole@hills.com', '2019-11-17 00:46:13', '2019-11-17 00:46:13'),
	(144, 2, 'Josue Maggio', '43645687', 'M', 'DNI', '999999999', 'buckridge.mina@hotmail.com', '2019-11-17 00:46:13', '2019-11-17 00:46:13'),
	(145, 2, 'Jettie Hayes', '89586816', 'F', 'DNI', '999999999', 'iconnelly@hotmail.com', '2019-11-17 00:46:13', '2019-11-17 00:46:13'),
	(146, 1, 'Dr. Maynard Blanda', '79001925', 'F', 'DNI', '999999999', 'daugherty.ewell@konopelski.info', '2019-11-17 00:46:13', '2019-11-17 00:46:13'),
	(147, 5, 'Mr. Alejandrin Reynolds', '36562936', 'M', 'DNI', '999999999', 'marc76@hotmail.com', '2019-11-17 00:46:13', '2019-11-17 00:46:13'),
	(148, 9, 'Salma Cummerata', '14983273', 'M', 'DNI', '999999999', 'areichel@breitenberg.com', '2019-11-17 00:46:13', '2019-11-17 00:46:13'),
	(149, 9, 'Tatyana Collins', '46666221', 'M', 'DNI', '999999999', 'hector.gorczany@gmail.com', '2019-11-17 00:46:14', '2019-11-17 00:46:14'),
	(150, 4, 'Mr. Alexander Bernhard DDS', '61028082', 'M', 'DNI', '999999999', 'oschaefer@yahoo.com', '2019-11-17 00:46:14', '2019-11-17 00:46:14'),
	(151, 5, 'Prof. Jade Armstrong', '47572526', 'M', 'DNI', '999999999', 'kub.tiana@wiegand.info', '2019-11-17 00:46:14', '2019-11-17 00:46:14'),
	(152, 6, 'Delores Streich', '70118650', 'F', 'DNI', '999999999', 'pagac.stephon@hotmail.com', '2019-11-17 00:46:14', '2019-11-17 00:46:14'),
	(153, 9, 'Rosendo O\'Connell', '87643733', 'M', 'DNI', '999999999', 'sauer.americo@hayes.com', '2019-11-17 00:46:14', '2019-11-17 00:46:14'),
	(154, 3, 'Angelo Farrell', '76796552', 'M', 'DNI', '999999999', 'mills.elias@gmail.com', '2019-11-17 00:46:14', '2019-11-17 00:46:14'),
	(155, 6, 'Ebony Spinka DDS', '49664819', 'F', 'DNI', '999999999', 'leilani22@hotmail.com', '2019-11-17 00:46:14', '2019-11-17 00:46:14'),
	(156, 8, 'Adolf Ritchie', '24262585', 'F', 'DNI', '999999999', 'elsie.cole@schneider.org', '2019-11-17 00:46:14', '2019-11-17 00:46:14'),
	(157, 1, 'Prof. Geovanni Mosciski IV', '32386473', 'F', 'DNI', '999999999', 'urolfson@thiel.net', '2019-11-17 00:46:14', '2019-11-17 00:46:14'),
	(158, 7, 'Coty Stark Sr.', '27441924', 'F', 'DNI', '999999999', 'wuckert.laurel@yahoo.com', '2019-11-17 00:46:14', '2019-11-17 00:46:14'),
	(159, 2, 'Gerardo Weimann', '73636074', 'F', 'DNI', '999999999', 'damon.emmerich@thompson.com', '2019-11-17 00:46:14', '2019-11-17 00:46:14'),
	(160, 8, 'Dannie McGlynn', '35212390', 'F', 'DNI', '999999999', 'nader.gabriella@stoltenberg.net', '2019-11-17 00:46:14', '2019-11-17 00:46:14'),
	(161, 6, 'Lacy Welch', '42285879', 'M', 'DNI', '999999999', 'hirthe.marcelle@gmail.com', '2019-11-17 00:46:14', '2019-11-17 00:46:14'),
	(162, 3, 'Miss Aditya West Sr.', '65038972', 'F', 'DNI', '999999999', 'deven.kuhn@yahoo.com', '2019-11-17 00:46:14', '2019-11-17 00:46:14'),
	(163, 4, 'Fae Strosin', '49097674', 'F', 'DNI', '999999999', 'grady.jo@pfannerstill.org', '2019-11-17 00:46:14', '2019-11-17 00:46:14'),
	(164, 5, 'Makayla O\'Reilly', '87297146', 'F', 'DNI', '999999999', 'rafaela.murray@leuschke.biz', '2019-11-17 00:46:15', '2019-11-17 00:46:15'),
	(165, 8, 'Frieda Casper I', '31992921', 'M', 'DNI', '999999999', 'terrence.mraz@spinka.com', '2019-11-17 00:46:15', '2019-11-17 00:46:15'),
	(166, 1, 'Harmony Howe I', '83581143', 'M', 'DNI', '999999999', 'ukuhic@yahoo.com', '2019-11-17 00:46:15', '2019-11-17 00:46:15'),
	(167, 1, 'Dr. Jamel Weissnat V', '25991632', 'F', 'DNI', '999999999', 'bkreiger@nolan.com', '2019-11-17 00:46:15', '2019-11-17 00:46:15'),
	(168, 10, 'Prof. Hazle Hayes III', '32684266', 'M', 'DNI', '999999999', 'ronny82@hotmail.com', '2019-11-17 00:46:15', '2019-11-17 00:46:15'),
	(169, 8, 'Prof. Reagan O\'Conner', '13343935', 'M', 'DNI', '999999999', 'zconroy@yahoo.com', '2019-11-17 00:46:15', '2019-11-17 00:46:15'),
	(170, 10, 'Sister Kuhn', '35356284', 'M', 'DNI', '999999999', 'kaitlin.ritchie@yahoo.com', '2019-11-17 00:46:15', '2019-11-17 00:46:15'),
	(171, 1, 'Antonetta Reynolds', '11077919', 'F', 'DNI', '999999999', 'morar.claud@hotmail.com', '2019-11-17 00:46:15', '2019-11-17 00:46:15'),
	(172, 3, 'Marjorie Stoltenberg', '67206394', 'M', 'DNI', '999999999', 'kurtis.schiller@bergnaum.biz', '2019-11-17 00:46:15', '2019-11-17 00:46:15'),
	(173, 10, 'Brittany McKenzie', '38518959', 'F', 'DNI', '999999999', 'cristal.torphy@doyle.com', '2019-11-17 00:46:15', '2019-11-17 00:46:15'),
	(174, 5, 'Domenic Lindgren', '85737856', 'F', 'DNI', '999999999', 'zhane@yahoo.com', '2019-11-17 00:46:15', '2019-11-17 00:46:15'),
	(175, 5, 'Idell Prosacco', '58898427', 'M', 'DNI', '999999999', 'cmertz@zemlak.net', '2019-11-17 00:46:15', '2019-11-17 00:46:15'),
	(176, 1, 'Stanford Nikolaus', '10263986', 'F', 'DNI', '999999999', 'mraz.joanny@kuhlman.biz', '2019-11-17 00:46:15', '2019-11-17 00:46:15'),
	(177, 3, 'Irma McGlynn', '86842135', 'M', 'DNI', '999999999', 'vernon75@gmail.com', '2019-11-17 00:46:15', '2019-11-17 00:46:15'),
	(178, 3, 'Helmer Schoen', '30826369', 'M', 'DNI', '999999999', 'armando33@zieme.net', '2019-11-17 00:46:15', '2019-11-17 00:46:15'),
	(179, 8, 'Dr. Javier Mertz Jr.', '22661655', 'F', 'DNI', '999999999', 'cwaters@zieme.net', '2019-11-17 00:46:15', '2019-11-17 00:46:15'),
	(180, 1, 'Maryjane Carter Sr.', '83217774', 'M', 'DNI', '999999999', 'justice.donnelly@hotmail.com', '2019-11-17 00:46:15', '2019-11-17 00:46:15'),
	(181, 5, 'Dr. Velma Jones', '22931089', 'F', 'DNI', '999999999', 'alisa.barton@windler.info', '2019-11-17 00:46:15', '2019-11-17 00:46:15'),
	(182, 10, 'Prof. Maximo Schamberger', '35878728', 'M', 'DNI', '999999999', 'eryn74@weissnat.com', '2019-11-17 00:46:15', '2019-11-17 00:46:15'),
	(183, 9, 'Josiah Casper', '82041463', 'M', 'DNI', '999999999', 'cremin.libbie@zulauf.com', '2019-11-17 00:46:16', '2019-11-17 00:46:16'),
	(184, 10, 'Jamie McLaughlin', '34227497', 'M', 'DNI', '999999999', 'payton32@hotmail.com', '2019-11-17 00:46:16', '2019-11-17 00:46:16'),
	(185, 4, 'Calista Graham', '36198021', 'M', 'DNI', '999999999', 'hreynolds@gmail.com', '2019-11-17 00:46:16', '2019-11-17 00:46:16'),
	(186, 8, 'Gabe O\'Reilly', '86808235', 'F', 'DNI', '999999999', 'nelle69@schultz.com', '2019-11-17 00:46:16', '2019-11-17 00:46:16'),
	(187, 7, 'Lois Fahey', '43735297', 'F', 'DNI', '999999999', 'emmanuelle21@lynch.net', '2019-11-17 00:46:16', '2019-11-17 00:46:16'),
	(188, 5, 'Jody Nicolas', '17158673', 'M', 'DNI', '999999999', 'auer.brendan@graham.org', '2019-11-17 00:46:16', '2019-11-17 00:46:16'),
	(189, 10, 'Dr. Jillian Goodwin DDS', '70853877', 'M', 'DNI', '999999999', 'sipes.mackenzie@gmail.com', '2019-11-17 00:46:16', '2019-11-17 00:46:16'),
	(190, 3, 'Dr. Demario Green', '17907140', 'M', 'DNI', '999999999', 'hartmann.loma@pfannerstill.biz', '2019-11-17 00:46:16', '2019-11-17 00:46:16'),
	(191, 7, 'Katharina Kris DDS', '49113506', 'M', 'DNI', '999999999', 'isaias05@yahoo.com', '2019-11-17 00:46:16', '2019-11-17 00:46:16'),
	(192, 9, 'Pedro Turcotte', '53232397', 'M', 'DNI', '999999999', 'gleason.keagan@langosh.net', '2019-11-17 00:46:16', '2019-11-17 00:46:16'),
	(193, 7, 'Dr. Marc Lubowitz', '66296745', 'F', 'DNI', '999999999', 'jakob07@reichel.com', '2019-11-17 00:46:16', '2019-11-17 00:46:16'),
	(194, 7, 'Mr. Jerod Walsh III', '47251383', 'M', 'DNI', '999999999', 'goodwin.marilou@runolfsdottir.net', '2019-11-17 00:46:16', '2019-11-17 00:46:16'),
	(195, 1, 'Kristopher Schultz', '36621315', 'M', 'DNI', '999999999', 'brianne34@braun.com', '2019-11-17 00:46:16', '2019-11-17 00:46:16'),
	(196, 8, 'Joelle Sipes DVM', '73655840', 'M', 'DNI', '999999999', 'bspinka@gmail.com', '2019-11-17 00:46:16', '2019-11-17 00:46:16'),
	(197, 9, 'Floy Kris V', '74125991', 'F', 'DNI', '999999999', 'icremin@yahoo.com', '2019-11-17 00:46:16', '2019-11-17 00:46:16'),
	(198, 3, 'Mr. Forest Reinger DDS', '24354740', 'F', 'DNI', '999999999', 'kristian13@yahoo.com', '2019-11-17 00:46:16', '2019-11-17 00:46:16'),
	(199, 4, 'Lillie Ward DDS', '42707228', 'M', 'DNI', '999999999', 'xbartell@yahoo.com', '2019-11-17 00:46:16', '2019-11-17 00:46:16'),
	(200, 4, 'Mr. Alberto Buckridge I', '75794674', 'M', 'DNI', '999999999', 'gleason.marilou@ondricka.info', '2019-11-17 00:46:16', '2019-11-17 00:46:16'),
	(201, 1, 'Luis Santamaria', '47952714', 'M', 'DNI', '943403849', 'ljonathan_45@hotmail.com', '2019-11-17 01:38:05', '2019-11-17 01:38:05'),
	(202, 2, 'Luis Santamaria', '87654321', 'M', 'DNI', '654321', '26', '2019-11-17 02:38:15', '2019-11-17 02:38:15');
/*!40000 ALTER TABLE `personas` ENABLE KEYS */;

-- Volcando estructura para tabla tablerocontrol.procesos
CREATE TABLE IF NOT EXISTS `procesos` (
  `id_proceso` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nombreProceso` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idt_proceso` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_proceso`),
  KEY `procesos_idt_proceso_foreign` (`idt_proceso`),
  CONSTRAINT `procesos_idt_proceso_foreign` FOREIGN KEY (`idt_proceso`) REFERENCES `tipo_procesos` (`idt_proceso`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla tablerocontrol.procesos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `procesos` DISABLE KEYS */;
INSERT INTO `procesos` (`id_proceso`, `nombreProceso`, `idt_proceso`, `created_at`, `updated_at`) VALUES
	(1, 'CONOCIMIENTO', 1, NULL, NULL),
	(2, 'EJECUCION', 1, NULL, NULL);
/*!40000 ALTER TABLE `procesos` ENABLE KEYS */;

-- Volcando estructura para tabla tablerocontrol.rol_personas
CREATE TABLE IF NOT EXISTS `rol_personas` (
  `idRol` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `rol` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idt_persona` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idRol`),
  KEY `rol_personas_idt_persona_foreign` (`idt_persona`),
  CONSTRAINT `rol_personas_idt_persona_foreign` FOREIGN KEY (`idt_persona`) REFERENCES `tipo_personas` (`idt_persona`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla tablerocontrol.rol_personas: ~10 rows (aproximadamente)
/*!40000 ALTER TABLE `rol_personas` DISABLE KEYS */;
INSERT INTO `rol_personas` (`idRol`, `rol`, `idt_persona`, `created_at`, `updated_at`) VALUES
	(1, 'Demandante', 1, '2019-11-17 00:46:05', '2019-11-17 00:46:05'),
	(2, 'Demandado', 1, '2019-11-17 00:46:05', '2019-11-17 00:46:05'),
	(3, 'Abogado', 1, '2019-11-17 00:46:05', '2019-11-17 00:46:05'),
	(4, 'Secretario', 1, '2019-11-17 00:46:05', '2019-11-17 00:46:05'),
	(5, 'Mason Rau', 1, '2019-11-17 00:46:05', '2019-11-17 00:46:05'),
	(6, 'Mia Stoltenberg', 1, '2019-11-17 00:46:05', '2019-11-17 00:46:05'),
	(7, 'Jannie Trantow', 1, '2019-11-17 00:46:05', '2019-11-17 00:46:05'),
	(8, 'Lura Nienow', 1, '2019-11-17 00:46:05', '2019-11-17 00:46:05'),
	(9, 'Drew Morar Jr.', 1, '2019-11-17 00:46:06', '2019-11-17 00:46:06'),
	(10, 'Randall Stiedemann', 1, '2019-11-17 00:46:06', '2019-11-17 00:46:06');
/*!40000 ALTER TABLE `rol_personas` ENABLE KEYS */;

-- Volcando estructura para tabla tablerocontrol.tipo_actividads
CREATE TABLE IF NOT EXISTS `tipo_actividads` (
  `idt_actividad` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idt_actividad`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla tablerocontrol.tipo_actividads: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `tipo_actividads` DISABLE KEYS */;
INSERT INTO `tipo_actividads` (`idt_actividad`, `descripcion`, `created_at`, `updated_at`) VALUES
	(1, 'Reunion externa', '2019-11-17 00:48:24', '2019-11-17 00:48:24'),
	(2, 'Pleito', '2019-11-17 00:48:39', '2019-11-17 00:48:39'),
	(3, 'Consulta', '2019-11-17 00:50:05', '2019-11-17 00:50:05');
/*!40000 ALTER TABLE `tipo_actividads` ENABLE KEYS */;

-- Volcando estructura para tabla tablerocontrol.tipo_personas
CREATE TABLE IF NOT EXISTS `tipo_personas` (
  `idt_persona` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idt_persona`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla tablerocontrol.tipo_personas: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `tipo_personas` DISABLE KEYS */;
INSERT INTO `tipo_personas` (`idt_persona`, `descripcion`, `created_at`, `updated_at`) VALUES
	(1, 'NATURAL', '2019-11-17 00:46:05', '2019-11-17 00:46:05');
/*!40000 ALTER TABLE `tipo_personas` ENABLE KEYS */;

-- Volcando estructura para tabla tablerocontrol.tipo_procesos
CREATE TABLE IF NOT EXISTS `tipo_procesos` (
  `idt_proceso` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idt_proceso`),
  UNIQUE KEY `tipo_procesos_descripcion_unique` (`descripcion`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla tablerocontrol.tipo_procesos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `tipo_procesos` DISABLE KEYS */;
INSERT INTO `tipo_procesos` (`idt_proceso`, `descripcion`, `created_at`, `updated_at`) VALUES
	(1, 'CONTENCIOSO', NULL, NULL),
	(2, 'NO CONTENCIOSO', NULL, NULL);
/*!40000 ALTER TABLE `tipo_procesos` ENABLE KEYS */;

-- Volcando estructura para tabla tablerocontrol.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla tablerocontrol.users: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Hollis Rohan Jr.', 'carmen32@example.com', '2019-11-17 00:46:05', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'CKVFnjAbNL', '2019-11-17 00:46:05', '2019-11-17 00:46:05'),
	(2, 'Luis Santa', 'jsantamaria2505@gmail.com', NULL, '$2y$10$rogyFfkfES7PseMsRqGYU.QqA12Cq178MC3Gi6TLKFmaLX5zCEd.6', NULL, '2019-11-17 00:47:55', '2019-11-17 00:47:55');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
