<?php include('../conexion/conexion.php'); ?>


<!---->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Esta seguro que desea salir?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Seleccione "Salir" para cerrar su sesion.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="../index.php">Salir</a>
          </div>
        </div>
      </div>
</div>

<!--Modal Nuevo Proceso-->
  <div class="modal" id="modal-new-process">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Nuevo Proceso</h4><br>
          
          
        </div>
        <form id="formLimpiar" >
        <!-- Modal body -->
        <div class="modal-body">

                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Nº Proc:</label>
                  <input type="text"  id="num_proceso" name="num_proceso" class="col-md-6  form-control" disabled>
                </div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Proceso:</label>
                  <select class="form-control col-md-6 process" id="proceso" name="proceso">
                      
                      <?php 
                        
                        $query = $conexion -> query ("SELECT * FROM proceso");
                              while ($valores = mysqli_fetch_array($query)) {
                                echo '<option value="'.$valores[idProceso].'">'.$valores[descProceso].'</option>';
                            }
                            
                       ?>
                  </select>
                </div>
                  <div class="col-md-2"></div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Tipo Proc:</label>
                  <div class=" col-md-6 without-padding" id="selectTipoProceso" >
                  </div>

                </div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Tipo Sol:</label>
                  <select class="form-control col-md-6" id="tipo_solicitante" name="tipo_solicitante">
                    <option value="1">Demandante</option>
                    <option value="2">Demandandado</option> 
                  </select>
                </div>

                <div class="row">
                  <div class="col-md-2"></div>
              	  <label class="label-texto col-md-2">Nombres:</label>
                  <input type="text"  id="nombre" name="nombre" class="col-md-6  form-control" placeholder="Ingrese Nombres" required>
                </div>

                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Apellidos:</label>
                  <input type="text"  id="apellido" name="apellido" class="col-md-6 form-control" placeholder="Ingrese Apellidos" required>
                </div>

                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Edad:</label>
                  <input type="text"  id="edad" name="edad" class="col-md-6 form-control" placeholder="Ingrese Edad" required>
                </div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Telefono:</label>
                 <input type="text" id="telefono" name="telefono" class="col-md-6 form-control" placeholder="Ingrese Nº Telefono" maxlength="9" required>
                </div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Direccion:</label>
                  <input type="text"  id="direccion" name="direccion" class="col-md-6 form-control" placeholder="Ingrese Direccion" required>
                </div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">DNI:</label>
                  <input type="text"  id="dni" name="dni" class="col-md-6 form-control" placeholder="Ingrese Documento de Identidad" required>
                </div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Distrito:</label>
                  <select class="form-control col-md-6" id="distrito" name="distrito">
                      <?php 
                        
                        $query = $conexion -> query ("SELECT * FROM distrito");
                              while ($valores = mysqli_fetch_array($query)) {
                                echo '<option value="'.$valores[idDistrito].'">'.$valores[descDistrito].'</option>';
                            }
                            
                       ?>
                  </select>
                </div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Juzgado:</label>
                  <select class="form-control col-md-6" id="juzgado" name="juzgado">
                      <?php 
                        
                        $query = $conexion -> query ("SELECT * FROM juzgado");
                              while ($valores = mysqli_fetch_array($query)) {
                                echo '<option value="'.$valores[idJuzgado].'">'.$valores[descJuzgado].'</option>';
                            }
                            
                       ?>
                  </select>
                </div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Abogado:</label>
                  <select class="form-control col-md-6" id="abogado" name="abogado">
                    <option value="1">Abogado a</option>
                    <option value="2">Abogado b</option> 
                  </select>
                </div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Fecha:</label>
                  <input type="datetime-local"  id="fecha" name="fecha" class="col-md-6 form-control" placeholder="Ingrese Distrito" required>
                </div>
                  	
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
         <button type="button" id="btnLimpiar" class="btn btn-info"  onclick='registrar_proceso();'>REGISTRAR</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">CERRAR</button>
        </div>
        </form>


          </div>
      </div>
  </div>
<!--Fin Modal-->

<!--Modal Nueva Persona-->
  <div class="modal" id="modal-new-person">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Nueva Persona</h4><br>
          
          
        </div>
        <form id="formLimpiarPerson" >
        <!-- Modal body -->
        <div class="modal-body">

                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Nº Proc:</label>
                  <input type="text"  id="num_proceso" name="num_proceso" class="col-md-6  form-control" disabled>
                </div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Proceso:</label>
                  <select class="form-control col-md-6 process" id="proceso" name="proceso">
                      
                      <?php 
                        
                        $query = $conexion -> query ("SELECT * FROM proceso");
                              while ($valores = mysqli_fetch_array($query)) {
                                echo '<option value="'.$valores[idProceso].'">'.$valores[descProceso].'</option>';
                            }
                            
                       ?>
                  </select>
                </div>
                  <div class="col-md-2"></div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Tipo Proc:</label>
                  <div class=" col-md-6 without-padding" id="selectTipoProceso" >
                  </div>

                </div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Tipo Sol:</label>
                  <select class="form-control col-md-6" id="tipo_solicitante" name="tipo_solicitante">
                    <option value="1">Demandante</option>
                    <option value="2">Demandandado</option> 
                  </select>
                </div>

                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Nombres:</label>
                  <input type="text"  id="nombre" name="nombre" class="col-md-6  form-control" placeholder="Ingrese Nombres" required>
                </div>

                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Apellidos:</label>
                  <input type="text"  id="apellido" name="apellido" class="col-md-6 form-control" placeholder="Ingrese Apellidos" required>
                </div>

                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Edad:</label>
                  <input type="text"  id="edad" name="edad" class="col-md-6 form-control" placeholder="Ingrese Edad" required>
                </div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Telefono:</label>
                 <input type="text" id="telefono" name="telefono" class="col-md-6 form-control" placeholder="Ingrese Nº Telefono" maxlength="9" required>
                </div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Direccion:</label>
                  <input type="text"  id="direccion" name="direccion" class="col-md-6 form-control" placeholder="Ingrese Direccion" required>
                </div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">DNI:</label>
                  <input type="text"  id="dni" name="dni" class="col-md-6 form-control" placeholder="Ingrese Documento de Identidad" required>
                </div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Distrito:</label>
                  <select class="form-control col-md-6" id="distrito" name="distrito">
                      <?php 
                        
                        $query = $conexion -> query ("SELECT * FROM distrito");
                              while ($valores = mysqli_fetch_array($query)) {
                                echo '<option value="'.$valores[idDistrito].'">'.$valores[descDistrito].'</option>';
                            }
                            
                       ?>
                  </select>
                </div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Juzgado:</label>
                  <select class="form-control col-md-6" id="juzgado" name="juzgado">
                      <?php 
                        
                        $query = $conexion -> query ("SELECT * FROM juzgado");
                              while ($valores = mysqli_fetch_array($query)) {
                                echo '<option value="'.$valores[idJuzgado].'">'.$valores[descJuzgado].'</option>';
                            }
                            
                       ?>
                  </select>
                </div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Abogado:</label>
                  <select class="form-control col-md-6" id="abogado" name="abogado">
                    <option value="1">Abogado a</option>
                    <option value="2">Abogado b</option> 
                  </select>
                </div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Fecha:</label>
                  <input type="datetime-local"  id="fecha" name="fecha" class="col-md-6 form-control" placeholder="Ingrese Distrito" required>
                </div>
                    
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
         <button type="button" id="btnLimpiarPersona" class="btn btn-info"  onclick='registrar_persona();'>REGISTRAR</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">CERRAR</button>
        </div>
        </form>


          </div>
      </div>
  </div>
<!--Fin Modal-->



  <!--Vista Orden Asignada-->
  <!--MODAL Activo-->
 <div class="modal" id="modal-vista-orden">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"></h4><br>
          
          
        </div>
        <form id="formLimpiar-2" >
        <!-- Modal body -->
        <div class="modal-body">

                <div class="row">
                  <div class="col-md-1"></div>
                  <label class="label-texto col-md-4">Asignacion</label>
                  <select class="form-control col-md-6" id="tipo_proceso" name="tipo_proceso">
                  </select>
                </div>

                <div class="row">
                  <div class="col-md-1"></div>
                  <label class="label-texto col-md-4">Envio de Escrito:</label>
                  <select class="form-control col-md-6" id="tipo_proceso" name="tipo_proceso">
                  </select>
                </div>
                <div class="row">
                  <div class="col-md-1"></div>
                  <label class="label-texto col-md-4">Nº Expediente:</label>
                  <input type="text"  id="nombre" name="nombre" class="col-md-6  form-control" required>
                </div>

                <div class="row">
                  <div class="col-md-1"></div>
                  <label class="label-texto col-md-4">Respuesta Juzgado:</label>
                  <select class="form-control col-md-6" id="tipo_proceso" name="tipo_proceso">
                  </select>
                </div>

                <div class="row">
                  <div class="col-md-1"></div>
                  <label class="label-texto col-md-4">Juez:</label>
                  <input type="text"  id="apellido" name="apellido" class="col-md-6 form-control" required>
                </div>
                    
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
         <button type="button" id="btnLimpiar" class="btn btn-info"  onclick='guardar_orden();'>REGISTRAR</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">CERRAR</button>
        </div>
        </form>


          </div>
      </div>
  </div>
  <!---->

<!--Vista Nueva Actividad-->
  <div class="modal" id="modal-nueva-actividad">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Nueva Actividad</h4><br>
          
          
        </div>
        <form id="formLimpiar-2" >
        <!-- Modal body -->
        <div class="modal-body">
                <div class="row">
                  <div class="col-md-1"></div>
                  <label class="label-texto col-md-4">Caso:</label>
                  <input type="text"  id="nombre" name="nombre" class="col-md-6  form-control" required>
                </div>

                <div class="row">
                  <div class="col-md-1"></div>
                  <label class="label-texto col-md-4">Nº Expediente:</label>
                  <input type="text"  id="nombre" name="nombre" class="col-md-6  form-control" required>
                </div>

                <div class="row">
                  <div class="col-md-1"></div>
                  <label class="label-texto col-md-4">Tipo Actividad</label>
                  <select class="form-control col-md-6" id="tipo_proceso" name="tipo_proceso">
                  </select>
                </div>

                <div class="row">
                  <div class="col-md-1"></div>
                  <label class="label-texto col-md-4">Juzgado:</label>
                  <input type="text"  id="nombre" name="nombre" class="col-md-6  form-control" required>
                </div>

                <div class="row">
                  <div class="col-md-1"></div>
                  <label class="label-texto col-md-4">Observacion 1:</label>
                  <input type="text"  id="nombre" name="nombre" class="col-md-6  form-control" required>
                </div>

                <div class="row">
                  <div class="col-md-1"></div>
                  <label class="label-texto col-md-4">Observacion 2:</label>
                  <textarea class="form-control col-md-6 " id="mensaje" name="mensaje" rows="5" required="required"></textarea>
                </div>

                    
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
         <button type="button" id="btnLimpiar" class="btn btn-info"  onclick='registrar_actividad();'>REGISTRAR</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">CERRAR</button>
        </div>
        </form>


          </div>
      </div>
  </div>
  <!---->
  
  <!--
  <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
  

	<script>
  
    
	   $("#btnLimpiar").click(function(event) {
	     $("#formLimpiar")[0].reset();
	   });
	</script>
  -->
	