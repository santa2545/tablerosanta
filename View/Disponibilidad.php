

<?php include('../conexion/conexion.php'); ?>

 <!DOCTYPE html>
<html lang="en">

<!-- begin::Head -->
<head>
    <base href="">
    <meta charset="utf-8" />
    <title>Tablero Control | Estudio de Abogados</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" href="../lib/alertify/css/alertify.min.css" />
    <link rel="stylesheet" href="../lib/alertify/css/themes/default.min.css" />
    <link rel="stylesheet" href="../lib/lineAwesome/css/line-awesome.min.css" />
    <link href="../assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/skins/header/base/light.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/skins/brand/navy.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/skins/aside/navy.css" rel="stylesheet" type="text/css" />
    <link href="../css/site.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="../assets/media/logos/favicon.ico" />
</head>

<!-- end::Head -->
<!-- begin::Body -->
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">


    <?php include 'header_movil.php' ?>

    <!-- begin:: Root -->
    <div class="kt-grid kt-grid--hor kt-grid--root">

        <!-- begin:: Page -->
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

            <?php include 'navAside.php' ?>

            <!-- begin:: Wrapper -->
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

                <?php include 'topHeader.php' ?>

                <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                    <?php include 'breadcrumb.php' ?>
                    
                    <!-- begin:: Content -->
                   <div class="container-fluid">
                        <div id="containerPrimary">
                        	<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head">
									<div class="kt-portlet__head-label">
										<h3 class="kt-portlet__head-title">
											Disponibilidad
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-toolbar">
											<div class="dropdown dropdown-inline">
												<button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													<i class="la la-sellsy"></i>
												</button>
												<div class="dropdown-menu dropdown-menu-right">
													<ul class="kt-nav">
														<li class="kt-nav__section kt-nav__section--first">
															<span class="kt-nav__section-text">Quick Actions</span>
														</li>
														<li class="kt-nav__item">
															<a href="#" class="kt-nav__link">
																<i class="kt-nav__link-icon flaticon2-graph-1"></i>
																<span class="kt-nav__link-text">Statistics</span>
															</a>
														</li>
														<li class="kt-nav__item">
															<a href="#" class="kt-nav__link">
																<i class="kt-nav__link-icon flaticon2-calendar-4"></i>
																<span class="kt-nav__link-text">Events</span>
															</a>
														</li>
														<li class="kt-nav__item">
															<a href="#" class="kt-nav__link">
																<i class="kt-nav__link-icon flaticon2-layers-1"></i>
																<span class="kt-nav__link-text">Reports</span>
															</a>
														</li>
														<li class="kt-nav__item">
															<a href="#" class="kt-nav__link">
																<i class="kt-nav__link-icon flaticon2-bell-1o"></i>
																<span class="kt-nav__link-text">Notifications</span>
															</a>
														</li>
														<li class="kt-nav__item">
															<a href="#" class="kt-nav__link">
																<i class="kt-nav__link-icon flaticon2-file-1"></i>
																<span class="kt-nav__link-text">Files</span>
															</a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">
									<div class="row">

										<div class="col-md-6">
											<label class="label-texto col-md-2">Proceso:</label>
											<select class="form-control col-md-6 process" id="proceso" name="proceso">
                      
                                                <?php 
                                                    
                                                    $query = $conexion -> query ("SELECT * FROM proceso");
                                                        while ($valores = mysqli_fetch_array($query)) {
                                                            echo '<option value="'.$valores[idProceso].'">'.$valores[descProceso].'</option>';
                                                        }
                                                        
                                                ?>
                                            </select>
										</div>
										<div class="col-md-6">
											<label class="label-texto col-md-2">Tipo:</label>
                                                <div class=" col-md-6 without-padding" id="selectTipoProceso" >
                                                </div>
										</div>

									</div>
									<!--begin: Datatable -->
									<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer"><div class="row"><div class="col-sm-12">
										<table class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline" id="kt_table_1" role="grid" aria-describedby="kt_table_1_info" style="width: 981px;">
											<thead>
											  	<tr role="row">
											  		
										            <th class="sorting_desc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 38.25px;" aria-sort="descending" aria-label="Order ID: activate to sort column ascending">Codigo</th>
										            <th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 53.25px;" aria-label="Country: activate to sort column ascending">Nombres</th>
										            <th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 63.25px;" aria-label="Ship Address: activate to sort column ascending">Casos Asignados</th>
										            <th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 67.25px;" aria-label="Company Agent: activate to sort column ascending">Aceptados</th>
										            <th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 78.25px;" aria-label="Company Name: activate to sort column ascending">No Aceptados</th>
										            <th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 39.25px;" aria-label="Ship Date: activate to sort column ascending">Adicionales</th>
										            <th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 48.25px;" aria-label="Status: activate to sort column ascending">Cupo Disponible</th>
										            
										        </tr>
											</thead>
														
											<tbody>					
												<tr role="row" class="odd">
												  	
												  	<td class="sorting_1">001</td>
												  	<td>Luis Santamaria</td>
												  	<td>0</td>
												  	<td>0</td>
												  	<td>0</td>
												  	<td>3</td>
												  	<td>10</td>
												  	
												</tr>
												<tr role="row" class="even">
												  	
												  	<td class="sorting_1">002</td>
												  	<td>George Lopez</td>
												  	<td>0</td>
												  	<td>0</td>
												  	<td>0</td>
												  	<td>2</td>
												  	<td>10</td>
												  	
												</tr>
											</tbody>
										
										</table>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12 col-md-5">
										<div class="dataTables_info" id="kt_table_1_info" role="status" aria-live="polite">Showing 1 to 10 of 40 entries
										</div>
									</div>
									<div class="col-sm-12 col-md-7 dataTables_pager">
										
										<div class="dataTables_paginate paging_simple_numbers" id="kt_table_1_paginate">
											<ul class="pagination">
												<li class="paginate_button page-item previous disabled" id="kt_table_1_previous">
													<a href="#" aria-controls="kt_table_1" data-dt-idx="0" tabindex="0" class="page-link"><i class="la la-angle-left"></i>
													</a>
												</li>
												<li class="paginate_button page-item active">
													<a href="#" aria-controls="kt_table_1" data-dt-idx="1" tabindex="0" class="page-link">1</a>
												</li>
												<li class="paginate_button page-item ">
													<a href="#" aria-controls="kt_table_1" data-dt-idx="2" tabindex="0" class="page-link">2</a>
												</li>
												<li class="paginate_button page-item ">
													<a href="#" aria-controls="kt_table_1" data-dt-idx="3" tabindex="0" class="page-link">3</a>
												</li>
												<li class="paginate_button page-item ">
													<a href="#" aria-controls="kt_table_1" data-dt-idx="4" tabindex="0" class="page-link">4</a>
												</li>
												<li class="paginate_button page-item next" id="kt_table_1_next">
													<a href="#" aria-controls="kt_table_1" data-dt-idx="5" tabindex="0" class="page-link"><i class="la la-angle-right"></i></a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
									<!--end: Datatable -->
								</div>
							</div>
                            
                        </div>
                        <div id="containerSecondary" class="d-none"></div>

                    </div>
                    
                    <!-- end:: Content -->
                </div>
                
                
                
            </div>

            <!-- end:: Wrapper -->
        </div>
        
        <!-- end:: Page -->
    </div>

    <!-- end:: Root -->
    

    <!-- end::Offcanvas Toolbar Quick Actions -->
    
    <!-- begin:: Scrolltop -->
    <div id="kt_scrolltop" class="kt-scrolltop">
        <i class="la la-arrow-up"></i>
    </div>

    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Esta seguro que desea salir?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Seleccione "Salir" para cerrar su sesion.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="../index.php">Salir</a>
          </div>
        </div>
      </div>
    </div>

    <!-- end::Demo Panel -->
    <!-- begin::Global Config(global config for global JS sciprts) -->
    <script>
        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#5d78ff",
                    "metal": "#c4c5d6",
                    "light": "#ffffff",
                    "accent": "#00c5dc",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995",
                    "focus": "#9816f4"
                },
                "base": {
                    "label": [
                        "#c5cbe3",
                        "#a1a8c3",
                        "#3d4465",
                        "#3e4466"
                    ],
                    "shape": [
                        "#f0f3ff",
                        "#d9dffa",
                        "#afb4d4",
                        "#646c9a"
                    ]
                }
            }
        };
    </script>

    <!-- end::Global Config -->
    <!--begin::Global Theme Bundle(used by all pages) -->
    <script src="../assets/plugins/global/plugins.bundle.js" type="text/javascript"></script>
    <script src="../assets/js/scripts.bundle.js" type="text/javascript"></script>
    <!--end::Global Theme Bundle -->
    <!--begin::Page Vendors(used by this page) -->
    <script src="../assets/plugins/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
    <!--end::Page Vendors -->
    <!--begin::Page Scripts(used by this page) -->
    <script src="../assets/js/pages/dashboard.js" type="text/javascript"></script>
    <script src="../lib/alertify/alertify.min.js"></script>
    <script src="../config/env.js"></script>
    <script src="../config/constant.js"></script>
    <script src="../js/helper/api.js"></script>
    <script src="../js/helper/popup.js"></script>
    <script src="../js/site.js" asp-append-version="true"></script>
    <script>
        $(document).ready(function(){
            //validarNumero();
         //console.log("estamos listo jquery");
        cargarProceso();
        elegirProceso();
        });
    </script>
</body>
<!-- end::Body -->
</html>


