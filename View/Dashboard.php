
 <!DOCTYPE html>
<html lang="en">

<!-- begin::Head -->
<head>
    <base href="">
    <meta charset="utf-8" />
    <title>Tablero Control | Estudio de Abogados</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="stylesheet" href="../lib/alertify/css/alertify.min.css" />
    <link rel="stylesheet" href="../lib/alertify/css/themes/default.min.css" />
    <link rel="stylesheet" href="../lib/lineAwesome/css/line-awesome.min.css" />
    <!--end::Fonts -->
    <!--begin::Page Vendors Styles(used by this page) -->
    <link href="../assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />

    <!--end::Page Vendors Styles -->
    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="../assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/style.bundle.css" rel="stylesheet" type="text/css" />

    <!--end::Global Theme Styles -->
    <!--begin::Layout Skins(used by all pages) -->
    <link href="../assets/css/skins/header/base/light.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/skins/brand/navy.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/skins/aside/navy.css" rel="stylesheet" type="text/css" />
    <link href="../css/site.css" rel="stylesheet" type="text/css" />

    <!--end::Layout Skins -->
    <link rel="shortcut icon" href="../assets/media/logos/favicon.ico" />
</head>

<!-- end::Head -->
<!-- begin::Body -->
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">

    <?php include 'ventanaModal.php' ?>

    <?php include 'header_movil.php' ?>

    <!-- begin:: Root -->
    <div class="kt-grid kt-grid--hor kt-grid--root">

        <!-- begin:: Page -->
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

            <?php include 'navAside.php' ?>

            <!-- begin:: Wrapper -->
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

                <?php include 'topHeader.php' ?>

                <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                    <?php include 'breadcrumb.php' ?>

                    <!-- begin:: Content -->
                   <div class="container-fluid">
                        <div id="containerPrimary">

                        </div>

                    </div>
                    
                    <!-- end:: Content -->
                </div>
                
                
                
            </div>

            <!-- end:: Wrapper -->
        </div>
        
        <!-- end:: Page -->
    </div>

    <!-- end:: Root -->
    

    <!-- end::Offcanvas Toolbar Quick Actions -->
    
    <!-- begin:: Scrolltop -->
    <div id="kt_scrolltop" class="kt-scrolltop">
        <i class="la la-arrow-up"></i>
    </div>





    <!-- end::Demo Panel -->
    <!-- begin::Global Config(global config for global JS sciprts) -->
    <script>
        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#5d78ff",
                    "metal": "#c4c5d6",
                    "light": "#ffffff",
                    "accent": "#00c5dc",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995",
                    "focus": "#9816f4"
                },
                "base": {
                    "label": [
                        "#c5cbe3",
                        "#a1a8c3",
                        "#3d4465",
                        "#3e4466"
                    ],
                    "shape": [
                        "#f0f3ff",
                        "#d9dffa",
                        "#afb4d4",
                        "#646c9a"
                    ]
                }
            }
        };
    </script>

    <!-- end::Global Config -->
    <!--begin::Global Theme Bundle(used by all pages) -->
    <script src="../assets/plugins/global/plugins.bundle.js" type="text/javascript"></script>
    <script src="../assets/js/scripts.bundle.js" type="text/javascript"></script>
    <!--end::Global Theme Bundle -->
    <!--begin::Page Vendors(used by this page) -->
    <script src="../assets/plugins/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
    <!--end::Page Vendors -->
    <!--begin::Page Scripts(used by this page) -->
    <script src="../assets/js/pages/dashboard.js" type="text/javascript"></script>
    <script src="../lib/alertify/alertify.min.js"></script>
    <script src="../config/env.js"></script>
    <script src="../config/constant.js"></script>
    <script src="../js/helper/api.js"></script>
    <script src="../js/helper/popup.js"></script>
    <script src="../js/site.js" asp-append-version="true"></script>

    @RenderSection("Scripts", required: false)
</body>
<!-- end::Body -->
</html>


