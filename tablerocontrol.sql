-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-11-2019 a las 17:27:37
-- Versión del servidor: 10.4.8-MariaDB
-- Versión de PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tablerocontrol`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividads`
--

CREATE TABLE `actividads` (
  `id_actividad` bigint(20) UNSIGNED NOT NULL,
  `duracion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descripcion` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idt_actividad` bigint(20) UNSIGNED NOT NULL,
  `id_persona` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `actividads`
--

INSERT INTO `actividads` (`id_actividad`, `duracion`, `descripcion`, `idt_actividad`, `id_persona`, `created_at`, `updated_at`) VALUES
(1, '02:30', NULL, 1, 1, '2019-11-17 05:51:41', '2019-11-17 05:51:41'),
(2, '01:00', NULL, 3, 201, '2019-11-17 06:39:00', '2019-11-17 06:39:00'),
(3, '02:00', NULL, 3, 201, '2019-11-24 21:20:47', '2019-11-24 21:20:47'),
(4, '02:00', NULL, 1, 1, '2019-11-24 21:20:57', '2019-11-24 21:20:57'),
(5, '02:00', NULL, 1, 1, '2019-11-24 21:26:26', '2019-11-24 21:26:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calendarios`
--

CREATE TABLE `calendarios` (
  `id_calendario` bigint(20) UNSIGNED NOT NULL,
  `tipo` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `asunto` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_juzgado` bigint(20) UNSIGNED NOT NULL,
  `id_persona` bigint(20) UNSIGNED NOT NULL,
  `id_nota` bigint(20) UNSIGNED NOT NULL,
  `id_expediente` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `calendarios`
--

INSERT INTO `calendarios` (`id_calendario`, `tipo`, `descripcion`, `asunto`, `id_juzgado`, `id_persona`, `id_nota`, `id_expediente`, `created_at`, `updated_at`) VALUES
(1, 'Tipo', 'Reunion externa', 'Citacion', 1, 1, 1, 1, '2019-11-24 15:28:20', '2019-11-24 15:28:20'),
(3, 'Tipo', 'Reunion', 'Asunto', 1, 201, 2, 3, '2019-11-24 21:14:56', '2019-11-24 21:14:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `distritos`
--

CREATE TABLE `distritos` (
  `id_distrito` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `distritos`
--

INSERT INTO `distritos` (`id_distrito`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'RIMAC', NULL, NULL),
(2, 'S.J.L', '2019-11-24 14:51:35', '2019-11-24 14:51:35'),
(3, 'Miraflores', '2019-11-24 21:06:59', '2019-11-24 21:06:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

CREATE TABLE `estados` (
  `id_estado` bigint(20) UNSIGNED NOT NULL,
  `descripcion` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `estados`
--

INSERT INTO `estados` (`id_estado`, `descripcion`, `created_at`, `updated_at`) VALUES
(1, 'EN PROCESO', NULL, NULL),
(2, 'EJECUTADO', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `expedientes`
--

CREATE TABLE `expedientes` (
  `id_expediente` bigint(20) UNSIGNED NOT NULL,
  `nro_exp` int(11) NOT NULL,
  `descripcion` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_persona` bigint(20) UNSIGNED NOT NULL,
  `id_proceso` bigint(20) UNSIGNED NOT NULL,
  `id_estado` bigint(20) UNSIGNED NOT NULL,
  `id_juzgado` bigint(20) UNSIGNED NOT NULL,
  `id_actividad` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `expedientes`
--

INSERT INTO `expedientes` (`id_expediente`, `nro_exp`, `descripcion`, `id_persona`, `id_proceso`, `id_estado`, `id_juzgado`, `id_actividad`, `created_at`, `updated_at`) VALUES
(1, 1, 'Pleito', 1, 1, 1, 1, 1, '2019-11-24 11:26:53', '2019-11-24 11:26:53'),
(2, 1, 'Pleito', 1, 1, 1, 1, 1, '2019-11-24 11:27:45', '2019-11-24 11:27:45'),
(3, 3, 'Observacion', 1, 1, 1, 1, 1, '2019-11-24 11:40:25', '2019-11-24 11:40:25'),
(4, 4, 'Nuevo Expediente', 202, 1, 1, 1, 1, '2019-11-24 21:14:07', '2019-11-24 21:14:07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `juzgados`
--

CREATE TABLE `juzgados` (
  `id_juzgado` bigint(20) UNSIGNED NOT NULL,
  `nombJuzgado` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_distrito` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `juzgados`
--

INSERT INTO `juzgados` (`id_juzgado`, `nombJuzgado`, `id_distrito`, `created_at`, `updated_at`) VALUES
(1, 'TCP', 1, NULL, NULL),
(2, 'Paz Letrado', 1, '2019-11-24 14:29:38', '2019-11-24 14:29:38'),
(3, 'Juzgado N', 1, '2019-11-24 14:29:50', '2019-11-24 14:29:50'),
(4, 'Juzgado Miraflores', 3, '2019-11-24 21:07:11', '2019-11-24 21:07:11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_11_16_161804_create_tipo_personas_table', 1),
(4, '2019_11_16_162256_create_rol_personas_table', 1),
(5, '2019_11_16_162732_create_personas_table', 1),
(6, '2019_11_16_163425_create_tipo_procesos_table', 1),
(7, '2019_11_16_163552_create_procesos_table', 1),
(8, '2019_11_16_163819_create_distritos_table', 1),
(9, '2019_11_16_164106_create_juzgados_table', 1),
(10, '2019_11_16_164342_create_estados_table', 1),
(11, '2019_11_16_164626_create_tipo_actividads_table', 1),
(12, '2019_11_16_165035_create_actividads_table', 1),
(13, '2019_11_16_165811_create_expedientes_table', 1),
(14, '2019_11_16_171509_create_notas_table', 1),
(15, '2019_11_16_171531_create_calendarios_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notas`
--

CREATE TABLE `notas` (
  `id_nota` bigint(20) UNSIGNED NOT NULL,
  `descripcion` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `notas`
--

INSERT INTO `notas` (`id_nota`, `descripcion`, `created_at`, `updated_at`) VALUES
(1, 'Observacion n', NULL, NULL),
(2, 'Nota 2', '2019-11-24 15:09:45', '2019-11-24 15:09:45'),
(3, 'Nota 3', '2019-11-24 21:14:23', '2019-11-24 21:14:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE `personas` (
  `id_persona` bigint(20) UNSIGNED NOT NULL,
  `idrol` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `documento` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `genero` enum('M','F','OTROS') COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_docu` enum('DNI','RUC','OTROS') COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `personas`
--

INSERT INTO `personas` (`id_persona`, `idrol`, `nombre`, `documento`, `genero`, `tipo_docu`, `telefono`, `email`, `created_at`, `updated_at`) VALUES
(1, 6, 'Sylvia Stroman', '18268313', 'M', 'DNI', '999999999', 'eankunding@hotmail.com', '2019-11-17 05:46:06', '2019-11-17 05:46:06'),
(4, 1, 'Zita Orn', '33242945', 'M', 'DNI', '999999999', 'mitchel43@gmail.com', '2019-11-17 05:46:07', '2019-11-17 05:46:07'),
(5, 2, 'Dwight Haag', '72425022', 'M', 'DNI', '999999999', 'mueller.dock@pollich.com', '2019-11-17 05:46:07', '2019-11-17 05:46:07'),
(7, 6, 'Miss Camille Walter', '47967874', 'F', 'DNI', '999999999', 'gframi@gmail.com', '2019-11-17 05:46:07', '2019-11-17 05:46:07'),
(8, 4, 'Prof. Agnes Ondricka III', '35831163', 'F', 'DNI', '999999999', 'darion83@mueller.com', '2019-11-17 05:46:07', '2019-11-17 05:46:07'),
(11, 3, 'Lambert Osinski', '34578149', 'M', 'DNI', '999999999', 'abbott.matilda@yahoo.com', '2019-11-17 05:46:07', '2019-11-17 05:46:07'),
(12, 3, 'Eulalia Buckridge Jr.', '66238028', 'M', 'DNI', '999999999', 'carlie91@bogisich.info', '2019-11-17 05:46:07', '2019-11-17 05:46:07'),
(13, 1, 'Elliot Bosco', '83772259', 'F', 'DNI', '999999999', 'pearline.sipes@schmidt.net', '2019-11-17 05:46:07', '2019-11-17 05:46:07'),
(15, 3, 'Elva Swift Jr.', '74662114', 'F', 'DNI', '999999999', 'helene.fisher@brekke.com', '2019-11-17 05:46:07', '2019-11-17 05:46:07'),
(16, 2, 'Ali Funk', '50356467', 'F', 'DNI', '999999999', 'ggulgowski@gmail.com', '2019-11-17 05:46:07', '2019-11-17 05:46:07'),
(18, 4, 'Max Maggio Sr.', '89776404', 'F', 'DNI', '999999999', 'missouri.funk@ohara.net', '2019-11-17 05:46:07', '2019-11-17 05:46:07'),
(19, 2, 'Dr. Jadyn Quitzon Jr.', '63691794', 'M', 'DNI', '999999999', 'marjory.skiles@hotmail.com', '2019-11-17 05:46:07', '2019-11-17 05:46:07'),
(20, 1, 'Dr. Liza Thompson', '87857685', 'M', 'DNI', '999999999', 'dach.cindy@yahoo.com', '2019-11-17 05:46:08', '2019-11-17 05:46:08'),
(21, 1, 'Linnie Rowe', '88483243', 'M', 'DNI', '999999999', 'ivolkman@hotmail.com', '2019-11-17 05:46:08', '2019-11-17 05:46:08'),
(25, 2, 'Sabina Ratke', '62436351', 'M', 'DNI', '999999999', 'pansy.swift@rath.org', '2019-11-17 05:46:08', '2019-11-17 05:46:08'),
(27, 4, 'Keanu Greenholt', '85567771', 'M', 'DNI', '999999999', 'jfeil@gmail.com', '2019-11-17 05:46:08', '2019-11-17 05:46:08'),
(29, 6, 'Jed Schmidt', '58776063', 'M', 'DNI', '999999999', 'marina26@walter.org', '2019-11-17 05:46:08', '2019-11-17 05:46:08'),
(30, 1, 'Lowell Skiles', '23703668', 'M', 'DNI', '999999999', 'haag.earlene@moen.com', '2019-11-17 05:46:08', '2019-11-17 05:46:08'),
(31, 6, 'Ambrose McKenzie', '65885855', 'M', 'DNI', '999999999', 'ashields@cronin.com', '2019-11-17 05:46:08', '2019-11-17 05:46:08'),
(34, 4, 'Ms. Josephine Feest', '87965641', 'F', 'DNI', '999999999', 'freda.howe@fritsch.com', '2019-11-17 05:46:08', '2019-11-17 05:46:08'),
(35, 1, 'Antonio Halvorson V', '71283531', 'F', 'DNI', '999999999', 'aracely.jones@wyman.org', '2019-11-17 05:46:08', '2019-11-17 05:46:08'),
(36, 1, 'Laurianne Willms', '26514746', 'F', 'DNI', '999999999', 'elinore.zulauf@yahoo.com', '2019-11-17 05:46:08', '2019-11-17 05:46:08'),
(37, 1, 'Jazmin Ratke', '48343452', 'F', 'DNI', '999999999', 'lgorczany@hotmail.com', '2019-11-17 05:46:08', '2019-11-17 05:46:08'),
(38, 6, 'Prof. Ethel Nienow Sr.', '31536884', 'F', 'DNI', '999999999', 'wwintheiser@gmail.com', '2019-11-17 05:46:08', '2019-11-17 05:46:08'),
(39, 6, 'Isabell Ryan DDS', '29767179', 'M', 'DNI', '999999999', 'zechariah.okon@hotmail.com', '2019-11-17 05:46:08', '2019-11-17 05:46:08'),
(43, 2, 'Griffin Hoeger', '23247306', 'M', 'DNI', '999999999', 'hettie23@labadie.com', '2019-11-17 05:46:09', '2019-11-17 05:46:09'),
(44, 4, 'Mrs. Bryana Keebler', '21764086', 'M', 'DNI', '999999999', 'albin80@johnston.biz', '2019-11-17 05:46:09', '2019-11-17 05:46:09'),
(46, 4, 'Brody Doyle', '56822632', 'F', 'DNI', '999999999', 'kilback.esmeralda@murphy.com', '2019-11-17 05:46:09', '2019-11-17 05:46:09'),
(48, 2, 'Troy Towne IV', '35645356', 'F', 'DNI', '999999999', 'meagan46@yahoo.com', '2019-11-17 05:46:09', '2019-11-17 05:46:09'),
(53, 3, 'Tyrese Zboncak MD', '53103008', 'M', 'DNI', '999999999', 'carmella.crist@murphy.com', '2019-11-17 05:46:09', '2019-11-17 05:46:09'),
(56, 1, 'Merlin Goodwin', '83495966', 'M', 'DNI', '999999999', 'hilpert.jenifer@dare.org', '2019-11-17 05:46:09', '2019-11-17 05:46:09'),
(57, 2, 'Assunta King', '10492816', 'F', 'DNI', '999999999', 'kulas.landen@watsica.com', '2019-11-17 05:46:09', '2019-11-17 05:46:09'),
(59, 2, 'Mr. Rudolph Labadie MD', '15495996', 'M', 'DNI', '999999999', 'lisette.heathcote@yahoo.com', '2019-11-17 05:46:09', '2019-11-17 05:46:09'),
(66, 4, 'Adrian Reinger', '29846810', 'F', 'DNI', '999999999', 'runte.trycia@hotmail.com', '2019-11-17 05:46:10', '2019-11-17 05:46:10'),
(68, 3, 'Colby Langosh Sr.', '27714734', 'M', 'DNI', '999999999', 'columbus.dickens@wunsch.info', '2019-11-17 05:46:10', '2019-11-17 05:46:10'),
(69, 6, 'Zackary Bergstrom', '16282250', 'F', 'DNI', '999999999', 'howe.alexis@gmail.com', '2019-11-17 05:46:10', '2019-11-17 05:46:10'),
(70, 3, 'Dayna Murazik I', '13316145', 'F', 'DNI', '999999999', 'christelle.koepp@gmail.com', '2019-11-17 05:46:10', '2019-11-17 05:46:10'),
(72, 6, 'Cynthia Cole', '62288555', 'M', 'DNI', '999999999', 'leanna.schowalter@yahoo.com', '2019-11-17 05:46:10', '2019-11-17 05:46:10'),
(73, 4, 'Marian Mosciski', '81275477', 'M', 'DNI', '999999999', 'krystal.ryan@hotmail.com', '2019-11-17 05:46:10', '2019-11-17 05:46:10'),
(75, 1, 'Broderick Robel', '42421301', 'F', 'DNI', '999999999', 'cronin.keira@langosh.info', '2019-11-17 05:46:10', '2019-11-17 05:46:10'),
(81, 2, 'Anahi Renner', '14101349', 'M', 'DNI', '999999999', 'mraz.elnora@dickinson.com', '2019-11-17 05:46:10', '2019-11-17 05:46:10'),
(83, 3, 'Giuseppe Johnston', '80922502', 'F', 'DNI', '999999999', 'napoleon50@hotmail.com', '2019-11-17 05:46:10', '2019-11-17 05:46:10'),
(86, 3, 'Kevin Koelpin', '29932901', 'M', 'DNI', '999999999', 'hermiston.anya@hotmail.com', '2019-11-17 05:46:11', '2019-11-17 05:46:11'),
(90, 3, 'Ms. Hassie Breitenberg', '36982537', 'M', 'DNI', '999999999', 'harris.breana@upton.com', '2019-11-17 05:46:11', '2019-11-17 05:46:11'),
(93, 3, 'Minerva White III', '24054143', 'F', 'DNI', '999999999', 'hharris@ohara.com', '2019-11-17 05:46:11', '2019-11-17 05:46:11'),
(95, 2, 'Elena Greenholt', '89368477', 'M', 'DNI', '999999999', 'norberto.marvin@yahoo.com', '2019-11-17 05:46:11', '2019-11-17 05:46:11'),
(96, 2, 'Gavin Turcotte PhD', '61373217', 'F', 'DNI', '999999999', 'hleuschke@hirthe.com', '2019-11-17 05:46:11', '2019-11-17 05:46:11'),
(97, 4, 'Dr. Chauncey Schamberger', '48803487', 'F', 'DNI', '999999999', 'lgreenfelder@parker.org', '2019-11-17 05:46:11', '2019-11-17 05:46:11'),
(101, 6, 'Prof. Bettye Krajcik II', '10591746', 'M', 'DNI', '999999999', 'simeon.gleichner@yahoo.com', '2019-11-17 05:46:11', '2019-11-17 05:46:11'),
(104, 4, 'Prof. Wiley White IV', '38045876', 'F', 'DNI', '999999999', 'kohler.coralie@bednar.com', '2019-11-17 05:46:11', '2019-11-17 05:46:11'),
(106, 1, 'Dr. Teagan Torphy', '25076448', 'F', 'DNI', '999999999', 'bechtelar.yazmin@hotmail.com', '2019-11-17 05:46:12', '2019-11-17 05:46:12'),
(107, 4, 'Bradford Little', '26861481', 'F', 'DNI', '999999999', 'brooklyn.homenick@gmail.com', '2019-11-17 05:46:12', '2019-11-17 05:46:12'),
(110, 6, 'Prof. Vance Boehm V', '67264503', 'F', 'DNI', '999999999', 'cyrus.schmidt@hotmail.com', '2019-11-17 05:46:12', '2019-11-17 05:46:12'),
(112, 4, 'Prof. Macey Funk V', '88512143', 'M', 'DNI', '999999999', 'cborer@hotmail.com', '2019-11-17 05:46:12', '2019-11-17 05:46:12'),
(116, 1, 'Harmon Cormier', '80844553', 'M', 'DNI', '999999999', 'jkozey@hotmail.com', '2019-11-17 05:46:12', '2019-11-17 05:46:12'),
(117, 1, 'Dr. Ena Barton DDS', '82926158', 'F', 'DNI', '999999999', 'bartell.terence@cummings.com', '2019-11-17 05:46:12', '2019-11-17 05:46:12'),
(118, 3, 'Tracy Lynch', '46541876', 'F', 'DNI', '999999999', 'reta.stracke@yahoo.com', '2019-11-17 05:46:12', '2019-11-17 05:46:12'),
(123, 6, 'Lindsey Price', '38772896', 'M', 'DNI', '999999999', 'hrunolfsson@bednar.net', '2019-11-17 05:46:12', '2019-11-17 05:46:12'),
(124, 4, 'Minnie King', '52065243', 'F', 'DNI', '999999999', 'afton71@gmail.com', '2019-11-17 05:46:12', '2019-11-17 05:46:12'),
(127, 2, 'Olaf Nolan', '60907029', 'M', 'DNI', '999999999', 'chance06@gmail.com', '2019-11-17 05:46:12', '2019-11-17 05:46:12'),
(128, 3, 'Prof. Lesly Sipes', '52723234', 'F', 'DNI', '999999999', 'qweissnat@romaguera.com', '2019-11-17 05:46:13', '2019-11-17 05:46:13'),
(130, 6, 'Elena Towne', '62275982', 'F', 'DNI', '999999999', 'marie.blanda@gmail.com', '2019-11-17 05:46:13', '2019-11-17 05:46:13'),
(131, 1, 'Kiley Quitzon', '22574019', 'M', 'DNI', '999999999', 'vandervort.junius@olson.com', '2019-11-17 05:46:13', '2019-11-17 05:46:13'),
(134, 4, 'Gregory Ortiz', '75534248', 'M', 'DNI', '999999999', 'pfeffer.kaitlin@cole.com', '2019-11-17 05:46:13', '2019-11-17 05:46:13'),
(135, 2, 'Susan Grady', '22132441', 'M', 'DNI', '999999999', 'mollie.jones@kshlerin.com', '2019-11-17 05:46:13', '2019-11-17 05:46:13'),
(136, 2, 'Dr. Anais Schmitt III', '42172827', 'F', 'DNI', '999999999', 'violet56@hotmail.com', '2019-11-17 05:46:13', '2019-11-17 05:46:13'),
(138, 2, 'Dario Connelly', '17635853', 'M', 'DNI', '999999999', 'ansley.kuhlman@ward.org', '2019-11-17 05:46:13', '2019-11-17 05:46:13'),
(141, 2, 'Elliot Abernathy', '10311605', 'M', 'DNI', '999999999', 'otto58@rippin.net', '2019-11-17 05:46:13', '2019-11-17 05:46:13'),
(143, 1, 'Santino Stark', '11311335', 'M', 'DNI', '999999999', 'jarrod.cole@hills.com', '2019-11-17 05:46:13', '2019-11-17 05:46:13'),
(144, 2, 'Josue Maggio', '43645687', 'M', 'DNI', '999999999', 'buckridge.mina@hotmail.com', '2019-11-17 05:46:13', '2019-11-17 05:46:13'),
(145, 2, 'Jettie Hayes', '89586816', 'F', 'DNI', '999999999', 'iconnelly@hotmail.com', '2019-11-17 05:46:13', '2019-11-17 05:46:13'),
(146, 1, 'Dr. Maynard Blanda', '79001925', 'F', 'DNI', '999999999', 'daugherty.ewell@konopelski.info', '2019-11-17 05:46:13', '2019-11-17 05:46:13'),
(150, 4, 'Mr. Alexander Bernhard DDS', '61028082', 'M', 'DNI', '999999999', 'oschaefer@yahoo.com', '2019-11-17 05:46:14', '2019-11-17 05:46:14'),
(152, 6, 'Delores Streich', '70118650', 'F', 'DNI', '999999999', 'pagac.stephon@hotmail.com', '2019-11-17 05:46:14', '2019-11-17 05:46:14'),
(154, 3, 'Angelo Farrell', '76796552', 'M', 'DNI', '999999999', 'mills.elias@gmail.com', '2019-11-17 05:46:14', '2019-11-17 05:46:14'),
(155, 6, 'Ebony Spinka DDS', '49664819', 'F', 'DNI', '999999999', 'leilani22@hotmail.com', '2019-11-17 05:46:14', '2019-11-17 05:46:14'),
(157, 1, 'Prof. Geovanni Mosciski IV', '32386473', 'F', 'DNI', '999999999', 'urolfson@thiel.net', '2019-11-17 05:46:14', '2019-11-17 05:46:14'),
(159, 2, 'Gerardo Weimann', '73636074', 'F', 'DNI', '999999999', 'damon.emmerich@thompson.com', '2019-11-17 05:46:14', '2019-11-17 05:46:14'),
(161, 6, 'Lacy Welch', '42285879', 'M', 'DNI', '999999999', 'hirthe.marcelle@gmail.com', '2019-11-17 05:46:14', '2019-11-17 05:46:14'),
(162, 3, 'Miss Aditya West Sr.', '65038972', 'F', 'DNI', '999999999', 'deven.kuhn@yahoo.com', '2019-11-17 05:46:14', '2019-11-17 05:46:14'),
(163, 4, 'Fae Strosin', '49097674', 'F', 'DNI', '999999999', 'grady.jo@pfannerstill.org', '2019-11-17 05:46:14', '2019-11-17 05:46:14'),
(166, 1, 'Harmony Howe I', '83581143', 'M', 'DNI', '999999999', 'ukuhic@yahoo.com', '2019-11-17 05:46:15', '2019-11-17 05:46:15'),
(167, 1, 'Dr. Jamel Weissnat V', '25991632', 'F', 'DNI', '999999999', 'bkreiger@nolan.com', '2019-11-17 05:46:15', '2019-11-17 05:46:15'),
(171, 1, 'Antonetta Reynolds', '11077919', 'F', 'DNI', '999999999', 'morar.claud@hotmail.com', '2019-11-17 05:46:15', '2019-11-17 05:46:15'),
(172, 3, 'Marjorie Stoltenberg', '67206394', 'M', 'DNI', '999999999', 'kurtis.schiller@bergnaum.biz', '2019-11-17 05:46:15', '2019-11-17 05:46:15'),
(176, 1, 'Stanford Nikolaus', '10263986', 'F', 'DNI', '999999999', 'mraz.joanny@kuhlman.biz', '2019-11-17 05:46:15', '2019-11-17 05:46:15'),
(177, 3, 'Irma McGlynn', '86842135', 'M', 'DNI', '999999999', 'vernon75@gmail.com', '2019-11-17 05:46:15', '2019-11-17 05:46:15'),
(178, 3, 'Helmer Schoen', '30826369', 'M', 'DNI', '999999999', 'armando33@zieme.net', '2019-11-17 05:46:15', '2019-11-17 05:46:15'),
(180, 1, 'Maryjane Carter Sr.', '83217774', 'M', 'DNI', '999999999', 'justice.donnelly@hotmail.com', '2019-11-17 05:46:15', '2019-11-17 05:46:15'),
(185, 4, 'Calista Graham', '36198021', 'M', 'DNI', '999999999', 'hreynolds@gmail.com', '2019-11-17 05:46:16', '2019-11-17 05:46:16'),
(190, 3, 'Dr. Demario Green', '17907140', 'M', 'DNI', '999999999', 'hartmann.loma@pfannerstill.biz', '2019-11-17 05:46:16', '2019-11-17 05:46:16'),
(195, 1, 'Kristopher Schultz', '36621315', 'M', 'DNI', '999999999', 'brianne34@braun.com', '2019-11-17 05:46:16', '2019-11-17 05:46:16'),
(198, 3, 'Mr. Forest Reinger DDS', '24354740', 'F', 'DNI', '999999999', 'kristian13@yahoo.com', '2019-11-17 05:46:16', '2019-11-17 05:46:16'),
(199, 4, 'Lillie Ward DDS', '42707228', 'M', 'DNI', '999999999', 'xbartell@yahoo.com', '2019-11-17 05:46:16', '2019-11-17 05:46:16'),
(200, 4, 'Mr. Alberto Buckridge I', '75794674', 'M', 'DNI', '999999999', 'gleason.marilou@ondricka.info', '2019-11-17 05:46:16', '2019-11-17 05:46:16'),
(201, 1, 'Luis Santamaria', '47952714', 'M', 'DNI', '943403849', 'ljonathan_45@hotmail.com', '2019-11-17 06:38:05', '2019-11-17 06:38:05'),
(202, 2, 'Luis Santamaria', '87654321', 'M', 'DNI', '654321', 'jsantamaria2505@gmail.com', '2019-11-17 07:38:15', '2019-11-17 07:38:15'),
(203, 6, 'Carhuancho', '98765432', 'M', 'DNI', '565656565', 'jcarhuancho@gmail.com', '2019-11-24 21:08:17', '2019-11-24 21:08:17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `procesos`
--

CREATE TABLE `procesos` (
  `id_proceso` bigint(20) UNSIGNED NOT NULL,
  `nombreProceso` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idt_proceso` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `procesos`
--

INSERT INTO `procesos` (`id_proceso`, `nombreProceso`, `idt_proceso`, `created_at`, `updated_at`) VALUES
(1, 'CONOCIMIENTO', 1, NULL, NULL),
(2, 'EJECUCION', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol_personas`
--

CREATE TABLE `rol_personas` (
  `idRol` bigint(20) UNSIGNED NOT NULL,
  `rol` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idt_persona` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `rol_personas`
--

INSERT INTO `rol_personas` (`idRol`, `rol`, `idt_persona`, `created_at`, `updated_at`) VALUES
(1, 'Demandante', 1, '2019-11-17 05:46:05', '2019-11-17 05:46:05'),
(2, 'Demandado', 1, '2019-11-17 05:46:05', '2019-11-17 05:46:05'),
(3, 'Abogado', 1, '2019-11-17 05:46:05', '2019-11-17 05:46:05'),
(4, 'Secretario', 1, '2019-11-17 05:46:05', '2019-11-17 05:46:05'),
(6, 'Juez', 1, '2019-11-17 05:46:05', '2019-11-17 05:46:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_actividads`
--

CREATE TABLE `tipo_actividads` (
  `idt_actividad` bigint(20) UNSIGNED NOT NULL,
  `descripcion` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tipo_actividads`
--

INSERT INTO `tipo_actividads` (`idt_actividad`, `descripcion`, `created_at`, `updated_at`) VALUES
(1, 'Reunion externa', '2019-11-17 05:48:24', '2019-11-17 05:48:24'),
(2, 'Pleito', '2019-11-17 05:48:39', '2019-11-17 05:48:39'),
(3, 'Consulta', '2019-11-17 05:50:05', '2019-11-17 05:50:05'),
(4, 'Prueba', '2019-11-24 21:19:09', '2019-11-24 21:19:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_personas`
--

CREATE TABLE `tipo_personas` (
  `idt_persona` bigint(20) UNSIGNED NOT NULL,
  `descripcion` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tipo_personas`
--

INSERT INTO `tipo_personas` (`idt_persona`, `descripcion`, `created_at`, `updated_at`) VALUES
(1, 'NATURAL', '2019-11-17 05:46:05', '2019-11-17 05:46:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_procesos`
--

CREATE TABLE `tipo_procesos` (
  `idt_proceso` bigint(20) UNSIGNED NOT NULL,
  `descripcion` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tipo_procesos`
--

INSERT INTO `tipo_procesos` (`idt_proceso`, `descripcion`, `created_at`, `updated_at`) VALUES
(1, 'CONTENCIOSO', NULL, NULL),
(2, 'NO CONTENCIOSO', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Hollis Rohan Jr.', 'carmen32@example.com', '2019-11-17 05:46:05', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'iGhucFDBUqzovx8aTTAuuG72r35JwbqqaTauYM8BGOHhfO8tJUSnnf66eREs', '2019-11-17 05:46:05', '2019-11-17 05:46:05'),
(2, 'Luis Santa', 'jsantamaria2505@gmail.com', NULL, '$2y$10$rogyFfkfES7PseMsRqGYU.QqA12Cq178MC3Gi6TLKFmaLX5zCEd.6', NULL, '2019-11-17 05:47:55', '2019-11-17 05:47:55');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `actividads`
--
ALTER TABLE `actividads`
  ADD PRIMARY KEY (`id_actividad`),
  ADD KEY `actividads_idt_actividad_foreign` (`idt_actividad`),
  ADD KEY `actividads_id_persona_foreign` (`id_persona`);

--
-- Indices de la tabla `calendarios`
--
ALTER TABLE `calendarios`
  ADD PRIMARY KEY (`id_calendario`),
  ADD KEY `calendarios_id_persona_foreign` (`id_persona`),
  ADD KEY `calendarios_id_juzgado_foreign` (`id_juzgado`),
  ADD KEY `calendarios_id_nota_foreign` (`id_nota`),
  ADD KEY `calendarios_id_expediente_foreign` (`id_expediente`);

--
-- Indices de la tabla `distritos`
--
ALTER TABLE `distritos`
  ADD PRIMARY KEY (`id_distrito`);

--
-- Indices de la tabla `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`id_estado`);

--
-- Indices de la tabla `expedientes`
--
ALTER TABLE `expedientes`
  ADD PRIMARY KEY (`id_expediente`),
  ADD KEY `expedientes_id_persona_foreign` (`id_persona`),
  ADD KEY `expedientes_id_proceso_foreign` (`id_proceso`),
  ADD KEY `expedientes_id_estado_foreign` (`id_estado`),
  ADD KEY `expedientes_id_juzgado_foreign` (`id_juzgado`),
  ADD KEY `expedientes_id_actividad_foreign` (`id_actividad`);

--
-- Indices de la tabla `juzgados`
--
ALTER TABLE `juzgados`
  ADD PRIMARY KEY (`id_juzgado`),
  ADD KEY `juzgados_id_distrito_foreign` (`id_distrito`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `notas`
--
ALTER TABLE `notas`
  ADD PRIMARY KEY (`id_nota`);

--
-- Indices de la tabla `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`id_persona`),
  ADD KEY `personas_idrol_foreign` (`idrol`);

--
-- Indices de la tabla `procesos`
--
ALTER TABLE `procesos`
  ADD PRIMARY KEY (`id_proceso`),
  ADD KEY `procesos_idt_proceso_foreign` (`idt_proceso`);

--
-- Indices de la tabla `rol_personas`
--
ALTER TABLE `rol_personas`
  ADD PRIMARY KEY (`idRol`),
  ADD KEY `rol_personas_idt_persona_foreign` (`idt_persona`);

--
-- Indices de la tabla `tipo_actividads`
--
ALTER TABLE `tipo_actividads`
  ADD PRIMARY KEY (`idt_actividad`);

--
-- Indices de la tabla `tipo_personas`
--
ALTER TABLE `tipo_personas`
  ADD PRIMARY KEY (`idt_persona`);

--
-- Indices de la tabla `tipo_procesos`
--
ALTER TABLE `tipo_procesos`
  ADD PRIMARY KEY (`idt_proceso`),
  ADD UNIQUE KEY `tipo_procesos_descripcion_unique` (`descripcion`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `actividads`
--
ALTER TABLE `actividads`
  MODIFY `id_actividad` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `calendarios`
--
ALTER TABLE `calendarios`
  MODIFY `id_calendario` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `distritos`
--
ALTER TABLE `distritos`
  MODIFY `id_distrito` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `estados`
--
ALTER TABLE `estados`
  MODIFY `id_estado` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `expedientes`
--
ALTER TABLE `expedientes`
  MODIFY `id_expediente` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `juzgados`
--
ALTER TABLE `juzgados`
  MODIFY `id_juzgado` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `notas`
--
ALTER TABLE `notas`
  MODIFY `id_nota` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `personas`
--
ALTER TABLE `personas`
  MODIFY `id_persona` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=204;

--
-- AUTO_INCREMENT de la tabla `procesos`
--
ALTER TABLE `procesos`
  MODIFY `id_proceso` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `rol_personas`
--
ALTER TABLE `rol_personas`
  MODIFY `idRol` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `tipo_actividads`
--
ALTER TABLE `tipo_actividads`
  MODIFY `idt_actividad` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tipo_personas`
--
ALTER TABLE `tipo_personas`
  MODIFY `idt_persona` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tipo_procesos`
--
ALTER TABLE `tipo_procesos`
  MODIFY `idt_proceso` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `actividads`
--
ALTER TABLE `actividads`
  ADD CONSTRAINT `actividads_id_persona_foreign` FOREIGN KEY (`id_persona`) REFERENCES `personas` (`id_persona`) ON UPDATE CASCADE,
  ADD CONSTRAINT `actividads_idt_actividad_foreign` FOREIGN KEY (`idt_actividad`) REFERENCES `tipo_actividads` (`idt_actividad`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `calendarios`
--
ALTER TABLE `calendarios`
  ADD CONSTRAINT `calendarios_id_expediente_foreign` FOREIGN KEY (`id_expediente`) REFERENCES `expedientes` (`id_expediente`) ON UPDATE CASCADE,
  ADD CONSTRAINT `calendarios_id_juzgado_foreign` FOREIGN KEY (`id_juzgado`) REFERENCES `juzgados` (`id_juzgado`) ON UPDATE CASCADE,
  ADD CONSTRAINT `calendarios_id_nota_foreign` FOREIGN KEY (`id_nota`) REFERENCES `notas` (`id_nota`) ON UPDATE CASCADE,
  ADD CONSTRAINT `calendarios_id_persona_foreign` FOREIGN KEY (`id_persona`) REFERENCES `personas` (`id_persona`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `expedientes`
--
ALTER TABLE `expedientes`
  ADD CONSTRAINT `expedientes_id_actividad_foreign` FOREIGN KEY (`id_actividad`) REFERENCES `actividads` (`id_actividad`) ON UPDATE CASCADE,
  ADD CONSTRAINT `expedientes_id_estado_foreign` FOREIGN KEY (`id_estado`) REFERENCES `estados` (`id_estado`) ON UPDATE CASCADE,
  ADD CONSTRAINT `expedientes_id_juzgado_foreign` FOREIGN KEY (`id_juzgado`) REFERENCES `juzgados` (`id_juzgado`) ON UPDATE CASCADE,
  ADD CONSTRAINT `expedientes_id_persona_foreign` FOREIGN KEY (`id_persona`) REFERENCES `personas` (`id_persona`) ON UPDATE CASCADE,
  ADD CONSTRAINT `expedientes_id_proceso_foreign` FOREIGN KEY (`id_proceso`) REFERENCES `procesos` (`id_proceso`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `juzgados`
--
ALTER TABLE `juzgados`
  ADD CONSTRAINT `juzgados_id_distrito_foreign` FOREIGN KEY (`id_distrito`) REFERENCES `distritos` (`id_distrito`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `personas`
--
ALTER TABLE `personas`
  ADD CONSTRAINT `personas_idrol_foreign` FOREIGN KEY (`idrol`) REFERENCES `rol_personas` (`idRol`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `procesos`
--
ALTER TABLE `procesos`
  ADD CONSTRAINT `procesos_idt_proceso_foreign` FOREIGN KEY (`idt_proceso`) REFERENCES `tipo_procesos` (`idt_proceso`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `rol_personas`
--
ALTER TABLE `rol_personas`
  ADD CONSTRAINT `rol_personas_idt_persona_foreign` FOREIGN KEY (`idt_persona`) REFERENCES `tipo_personas` (`idt_persona`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
