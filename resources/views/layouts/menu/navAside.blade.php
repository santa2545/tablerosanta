<!-- begin:: Aside -->
<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
<div class="kt-aside  kt-aside--fixed bg-color-sidebar" id="kt_aside">

    <div class="kt-aside__brand kt-grid__item bg-color-sidebar" id="kt_aside_brand">
        <div class="kt-aside__brand-logo">
            <a href="#">
                <!--<img alt="Logo" src="img/logo-setra.jpg" style="width:88px; height:50px;"/>-->
            </a>
        </div>
        <div class="kt-aside__brand-tools">
            <button class="kt-aside__brand-aside-toggler kt-aside__brand-aside-toggler--left" id="kt_aside_toggler"><span></span></button>
        </div>
    </div>

    <div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
        <div id="kt_aside_menu" class="kt-aside-menu bg-color-sidebar" data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
            <ul class="kt-menu__nav ">
                <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--open kt-menu__item--here" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                    <a href="{{ route('home')}}" class="kt-menu__link bg-color-sidebar"><i class="kt-menu__link-icon flaticon2-graphic"></i><span class="kt-menu__link-text">Dashboards</span></a>
                </li>
                <li class="kt-menu__section ">
                    <h4 class="kt-menu__section-text colorTituloGrupal">Recepcion</h4>
                    <i class="kt-menu__section-icon flaticon-more-v2"></i>
                </li>

                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                    <a href="{{ route('distritos.index')}}" class="kt-menu__link"><i class="kt-menu__link-icon flaticon2-copy">
                        </i><span class="kt-menu__link-text">Distrito</span></a>
                    <div class="kt-menu__submenu ">
                        <span class="kt-menu__arrow"></span>
                    </div>
                </li>

                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                    <a href="{{ route('juzgados.index')}}" class="kt-menu__link"><i class="kt-menu__link-icon flaticon2-copy">
                        </i><span class="kt-menu__link-text">Juzgado</span></a>
                    <div class="kt-menu__submenu ">
                        <span class="kt-menu__arrow"></span>
                    </div>
                </li>

                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                    <a href="{{ route('tipo_actividades.index')}}" class="kt-menu__link"><i class="kt-menu__link-icon flaticon2-copy">
                        </i><span class="kt-menu__link-text">Tipo Actividad</span></a>
                    <div class="kt-menu__submenu ">
                        <span class="kt-menu__arrow"></span>
                    </div>
                </li>

                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                    <a href="{{ route('personas.index')}}" class="kt-menu__link"><i class="kt-menu__link-icon flaticon2-copy"></i><span class="kt-menu__link-text">Persona</span></a>
                    <div class="kt-menu__submenu ">
                        <span class="kt-menu__arrow"></span>
                    </div>
                </li>

                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                    <a href="{{ route('actividades.index')}}" class="kt-menu__link"><i class="kt-menu__link-icon flaticon2-copy">
                        </i><span class="kt-menu__link-text">Actividad</span></a>
                    <div class="kt-menu__submenu ">
                        <span class="kt-menu__arrow"></span>
                    </div>
                </li>

                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                    <a href="{{ route('expedientes.index')}}" class="kt-menu__link"><i class="kt-menu__link-icon flaticon2-copy"></i><span class="kt-menu__link-text">Expediente</span></a>
                    <div class="kt-menu__submenu ">
                        <span class="kt-menu__arrow"></span>
                    </div>
                </li>

                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                    <a href="{{ route('notas.index')}}" class="kt-menu__link"><i class="kt-menu__link-icon flaticon2-copy"></i><span class="kt-menu__link-text">Nota</span></a>
                    <div class="kt-menu__submenu ">
                        <span class="kt-menu__arrow"></span>
                    </div>
                </li>

                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                    <a href="{{ route('calendarios.index')}}" class="kt-menu__link"><i class="kt-menu__link-icon flaticon2-copy"></i><span class="kt-menu__link-text">Calendario</span></a>
                    <div class="kt-menu__submenu ">
                        <span class="kt-menu__arrow"></span>
                    </div>
                </li>

            </ul>
        </div>
    </div>

</div>
<!-- end:: Aside -->