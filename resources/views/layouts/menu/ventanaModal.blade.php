<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Esta seguro que desea salir?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Seleccione "Salir" para cerrar su sesion.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="dropdown-item" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                {{ __('SALIR') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
           <!-- <a class="btn btn-primary" href="#">Salir</a> -->
          </div>
        </div>
      </div>
</div>

<!--Modal Nueva Persona-->
  <div class="modal" id="modal-new-person">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Nueva Persona</h4><br>


        </div>
        <form method="POST" action="{{ route('personas.store')}}">
            @csrf
        <!-- Modal body -->
        <div class="modal-body">

                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Rol:</label>
                  <select class="form-control col-md-6 process" id="idrol" name="idrol">
                    @foreach (App\RolPersona::all() as $item)
                    <option value="{{$item->idRol}}">{{$item->rol}}</option>
                    @endforeach
                    
                  </select>
                      <div class="col-md-2">
           
                      </div>
                </div>
                
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Nombre:</label>
                  <input type="text"  id="nombre" name="nombre" class="col-md-6  form-control" placeholder="Ingrese Nombres">
                  <div class="col-md-2"></div>
                </div>
                
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Documento:</label>
                  <input type="text"  id="documento" name="documento" class="col-md-6  form-control" placeholder="Ingrese Documento">
                  <div class="col-md-2"></div>
                </div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Genero:</label>
                  <select class="form-control col-md-6" id="genero" name="genero">
                     
                      <option value="{{App\Persona::GENERO_M}}">MASCULINO</option>
                      <option value="{{App\Persona::GENERO_F}}">FEMENINO</option>
                      <option value="{{App\Persona::GENERO_OTROS}}">OTROS</option>
                  </select>
                </div>

                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Tipo Documento:</label>
                  <select class="form-control col-md-6" id="tipo_docu" name="tipo_docu">
                      <option value="{{App\Persona::TIPO_DOCU_DNI}}">DNI</option>
                      <option value="{{App\Persona::TIPO_DOCU_RUC}}">RUC</option>
                      <option value="{{App\Persona::TIPO_DOCU_OTROS}}">OTROS</option>
                    </select>
                </div>

                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Telefono:</label>
                  <input type="text"  id="telefono" name="telefono" class="col-md-6 form-control" placeholder="Ingrese Telefono">
                </div>

                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Email:</label>
                  <input type="text"  id="email" name="email" class="col-md-6 form-control" placeholder="Ingrese Email" required>
                </div>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
         <button type="submit" id="btnLimpiarPersona" class="btn btn-info">REGISTRAR</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">CERRAR</button>
        </div>
        </form>


          </div>
      </div>
  </div>
<!--Fin Modal-->

<!--Modal Nuevo Tipo Actividad-->
<div class="modal" id="modal-tipo-actividad">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Nuevo Tipo Actividad</h4><br>


        </div>
        <form id="formLimpiarPerson" method="POST" action="{{ route('tipo_actividades.store')}}">
            @csrf
        <!-- Modal body -->
        <div class="modal-body">
                
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Descripcion:</label>
                  <input type="text"  id="descripcion" name="descripcion" class="col-md-6  form-control" placeholder="Ingrese Descripcion">
                  <div class="col-md-2"></div>
                </div>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
         <button type="submit" id="btnLimpiarPersona" class="btn btn-info">REGISTRAR</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">CERRAR</button>
        </div>
        </form>


          </div>
      </div>
  </div>
<!--Fin Modal-->

<!--Modal Nueva Actividad-->
<div class="modal" id="modal-actividad">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Nueva Actividad</h4><br>


        </div>
        <form method="POST" action="{{ route('actividades.store')}}">
            @csrf
        <!-- Modal body -->
        <div class="modal-body">
                
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Duracion:</label>
                  <input type="time"  id="duracion" name="duracion" class="col-md-6  form-control" placeholder="Ingrese Descripcion">
                  <div class="col-md-2"></div>
                </div>

                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Tipo Actividad:</label>
                  <select class="form-control col-md-6 process" id="idt_actividad" name="idt_actividad">
                      @foreach (App\TipoActividad::all() as $item)
                      <option value="{{$item->idt_actividad}}">{{$item->descripcion}}</option>
                      @endforeach
                      
                    </select>
                  <div class="col-md-2"></div>
                </div>

                <div class="row">
                    <div class="col-md-2"></div>
                    <label class="label-texto col-md-2">Persona:</label>
                    <select class="form-control col-md-6 process" id="id_persona" name="id_persona">
                        @foreach (App\Persona::all() as $item)
                        <option value="{{$item->id_persona}}">{{$item->nombre}}</option>
                        @endforeach
                        
                      </select>
                    <div class="col-md-2"></div>
                  </div>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
         <button type="submit" id="btnLimpiarPersona" class="btn btn-info">REGISTRAR</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">CERRAR</button>
        </div>
        </form>


          </div>
      </div>
  </div>
<!--Fin Modal-->

<!--Modal Nuevo Expediente-->
<div class="modal" id="modal-nuevo-expediente">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Nuevo Expediente</h4><br>

      </div>
      <form method="POST" action="{{ route('expedientes.store')}}">
          @csrf
      <!-- Modal body -->
      <div class="modal-body">
              
              <div class="row">
                <div class="col-md-2"></div>
                <label class="label-texto col-md-2">Nro Expediente:</label>
                <input type="text"  id="nro_exp" name="nro_exp" class="col-md-6  form-control" placeholder="Ingrese Nro Expediente">
                <div class="col-md-2"></div>
              </div>

              <div class="row">
                <div class="col-md-2"></div>
                <label class="label-texto col-md-2">Descripcion:</label>
                <input type="text"  id="descripcion" name="descripcion" class="col-md-6  form-control" placeholder="Ingrese Descripcion">
                <div class="col-md-2"></div>
              </div>

              <div class="row">
                  <div class="col-md-2"></div>
                    <label class="label-texto col-md-2">Persona:</label>
                    <select class="form-control col-md-6 process" id="id_persona" name="id_persona">
                      @foreach (App\Persona::all() as $item)
                      <option value="{{$item->id_persona}}">{{$item->nombre}}</option>
                      @endforeach
                    </select>
                  <div class="col-md-2"></div>
              </div>

              <div class="row">
                  <div class="col-md-2"></div>
                    <label class="label-texto col-md-2">Proceso:</label>
                    <select class="form-control col-md-6 process" id="id_proceso" name="id_proceso">
                      @foreach (App\Proceso::all() as $item)
                      <option value="{{$item->id_proceso}}">{{$item->nombreProceso}}</option>
                      @endforeach
                    </select>
                  <div class="col-md-2"></div>
              </div>

              <div class="row">
                  <div class="col-md-2"></div>
                    <label class="label-texto col-md-2">Estado:</label>
                    <select class="form-control col-md-6 process" id="id_estado" name="id_estado">
                      @foreach (App\Estado::all() as $item)
                      <option value="{{$item->id_estado}}">{{$item->descripcion}}</option>
                      @endforeach
                      
                    </select>
                  <div class="col-md-2"></div>
              </div>

              <div class="row">
                  <div class="col-md-2"></div>
                    <label class="label-texto col-md-2">Juzgado:</label>
                    <select class="form-control col-md-6 process" id="id_juzgado" name="id_juzgado">
                      @foreach (App\Juzgado::all() as $item)
                      <option value="{{$item->id_juzgado}}">{{$item->nombJuzgado}}</option>
                      @endforeach
                      
                    </select>
                  <div class="col-md-2"></div>
              </div>

              <div class="row">
                  <div class="col-md-2"></div>
                    <label class="label-texto col-md-2">Actividad:</label>
                    <select class="form-control col-md-6 process" id="id_actividad" name="id_actividad">
                      @foreach (App\Actividad::all() as $item)
                      <option value="{{$item->id_actividad}}">{{$item->TipoActividad->descripcion}}</option>
                      @endforeach
                      
                    </select>
                  <div class="col-md-2"></div>
              </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="submit" id="btnLimpiarPersona" class="btn btn-info">REGISTRAR</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">CERRAR</button>
      </div>
      </form>


    </div>
  </div>
</div>
<!--Fin Modal-->

<!--Modal Calendario-->
<div class="modal" id="modal-calendario">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Nuevo Calendario</h4><br>

      </div>
      <form method="POST" action="{{ route('calendarios.store')}}">
          @csrf
      <!-- Modal body -->
      <div class="modal-body">
              
              <div class="row">
                <div class="col-md-2"></div>
                <label class="label-texto col-md-2">Tipo:</label>
                <input type="text"  id="tipo" name="tipo" class="col-md-6  form-control" placeholder="Ingrese Tipo">
                <div class="col-md-2"></div>
              </div>

              <div class="row">
                <div class="col-md-2"></div>
                <label class="label-texto col-md-2">Descripcion:</label>
                <input type="text"  id="descripcion" name="descripcion" class="col-md-6  form-control" placeholder="Ingrese Descripcion">
                <div class="col-md-2"></div>
              </div>

              <div class="row">
                  <div class="col-md-2"></div>
                    <label class="label-texto col-md-2">Asunto:</label>
                    <input type="text"  id="asunto" name="asunto" class="col-md-6  form-control" placeholder="Ingrese Asunto">
                  <div class="col-md-2"></div>
              </div>

              <div class="row">
                  <div class="col-md-2"></div>
                    <label class="label-texto col-md-2">Juzgado:</label>
                    <select class="form-control col-md-6 process" id="id_juzgado" name="id_juzgado">
                      @foreach (App\Juzgado::all() as $item)
                      <option value="{{$item->id_juzgado}}">{{$item->nombJuzgado}}</option>
                      @endforeach
                    </select>
                  <div class="col-md-2"></div>
              </div>

              <div class="row">
                  <div class="col-md-2"></div>
                    <label class="label-texto col-md-2">Persona:</label>
                    <select class="form-control col-md-6 process" id="id_persona" name="id_persona">
                      @foreach (App\Persona::all() as $item)
                      <option value="{{$item->id_persona}}">{{$item->nombre}}</option>
                      @endforeach
                      
                    </select>
                  <div class="col-md-2"></div>
              </div>

              <div class="row">
                  <div class="col-md-2"></div>
                    <label class="label-texto col-md-2">Nota:</label>
                    <select class="form-control col-md-6 process" id="id_nota" name="id_nota">
                      @foreach (App\Nota::all() as $item)
                      <option value="{{$item->id_nota}}">{{$item->descripcion}}</option>
                      @endforeach
                      
                    </select>
                  <div class="col-md-2"></div>
              </div>

              <div class="row">
                  <div class="col-md-2"></div>
                    <label class="label-texto col-md-2">Expediente:</label>
                    <select class="form-control col-md-6 process" id="id_expediente" name="id_expediente">
                      @foreach (App\Expediente::all() as $item)
                      <option value="{{$item->id_expediente}}">{{$item->descripcion}}</option>
                      @endforeach
                      
                    </select>
                  <div class="col-md-2"></div>
              </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="submit" id="btnLimpiarPersona" class="btn btn-info">REGISTRAR</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">CERRAR</button>
      </div>
      </form>


    </div>
  </div>
</div>
<!--Fin Modal-->

<!--Modal Nota-->
<div class="modal" id="modal-nota">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Nueva Nota</h4><br>

      </div>
      <form method="POST" action="{{ route('notas.store')}}">
          @csrf
      <!-- Modal body -->
      <div class="modal-body">
              
              <div class="row">
                <div class="col-md-2"></div>
                <label class="label-texto col-md-2">Descripcion:</label>
                <input type="text"  id="descripcion" name="descripcion" class="col-md-6  form-control" placeholder="Ingrese Nota">
                <div class="col-md-2"></div>
              </div>

      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="submit" id="btnLimpiarPersona" class="btn btn-info">REGISTRAR</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">CERRAR</button>
      </div>
      </form>


    </div>
  </div>
</div>
<!--Fin Modal-->

<!--Modal Distrito-->
<div class="modal" id="modal-distrito">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Nuevo Distrito</h4><br>

      </div>
      <form method="POST" action="{{ route('distritos.store')}}">
          @csrf
      <!-- Modal body -->
      <div class="modal-body">
              
              <div class="row">
                <div class="col-md-2"></div>
                <label class="label-texto col-md-2">Nombre:</label>
                <input type="text"  id="nombre" name="nombre" class="col-md-6  form-control" placeholder="Ingrese Distrito">
                <div class="col-md-2"></div>
              </div>

      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="submit" id="btnLimpiarPersona" class="btn btn-info">REGISTRAR</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">CERRAR</button>
      </div>
      </form>


    </div>
  </div>
</div>
<!--Fin Modal-->

<!--Modal Juzgado-->
<div class="modal" id="modal-juzgado">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Nuevo Juzgado</h4><br>

      </div>
      <form method="POST" action="{{ route('juzgados.store')}}">
          @csrf
      <!-- Modal body -->
      <div class="modal-body">
              
              <div class="row">
                <div class="col-md-2"></div>
                <label class="label-texto col-md-2">Nombre:</label>
                <input type="text"  id="nombJuzgado" name="nombJuzgado" class="col-md-6  form-control" placeholder="Ingrese Nombre">
                <div class="col-md-2"></div>
              </div>

              <div class="row">
                  <div class="col-md-2"></div>
                    <label class="label-texto col-md-2">Distrito:</label>
                    <select class="form-control col-md-6 process" id="id_distrito" name="id_distrito">
                      @foreach (App\Distrito::all() as $item)
                      <option value="{{$item->id_distrito}}">{{$item->nombre}}</option>
                      @endforeach
                    </select>
                  <div class="col-md-2"></div>
              </div>

      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="submit" id="btnLimpiarPersona" class="btn btn-info">REGISTRAR</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">CERRAR</button>
      </div>
      </form>


    </div>
  </div>
</div>
<!--Fin Modal-->