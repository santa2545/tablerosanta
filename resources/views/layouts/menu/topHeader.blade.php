<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">

        <!-- begin:: Header Menu -->
        <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
        <div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">

        </div>

        <!-- end:: Header Menu -->
        <!-- begin:: Header Topbar -->
        <div class="kt-header__topbar">
            <!--begin:: Languages -->
            <div class="kt-header__topbar-item kt-header__topbar-item--langs">
                <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                    <span class="kt-header__topbar-icon kt-rounded-">
                        <img class="" src="{{asset('assets/media/flags/020-flag.svg')}}" alt="" />
                    </span>
                </div>
                <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround">
                    <ul class="kt-nav kt-margin-t-10 kt-margin-b-10">
                        <li class="kt-nav__item">
                            <a href="#" class="kt-nav__link">
                                <span class="kt-nav__link-icon"><img src="{{asset('assets/media/flags/016-spain.svg')}}" alt="" /></span>
                                <span class="kt-nav__link-text">Spanish</span>
                            </a>
                        </li>
                    </ul>
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else

                        @endguest
                    </ul>
                </div>
            </div>

            <!--end:: Languages -->
            <!--begin: User Bar -->
            <div class="kt-header__topbar-item kt-header__topbar-item--user">
                <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">

                    <!--use "kt-rounded" class for rounded avatar style-->
                    <div class="kt-header__topbar-user kt-rounded-">
                        <span class="kt-header__topbar-welcome kt-hidden-mobile">Hi,</span>
                        <span class="kt-header__topbar-username kt-hidden-mobile">
                              <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                              {{ Auth::user()->name }}
                        @endguest

                          <span class="caret"></span></span>
                        <img alt="Pic" src="{{asset('assets/media/users/default.jpg')}}" class="kt-rounded-" />

                        <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                        <span class="kt-badge kt-badge--username kt-badge--lg kt-badge--brand kt-hidden kt-badge--bold">S</span>
                    </div>
                </div>
                <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-sm">
                    <div class="kt-user-card kt-margin-b-40 kt-margin-b-30-tablet-and-mobile">
                        <div class="kt-user-card__wrapper">
                            <div class="kt-user-card__pic">

                                <!--use "kt-rounded" class for rounded avatar style-->
                                <img alt="Pic" src="{{asset('assets/media/users/default.jpg')}}" class="kt-rounded-" />
                            </div>
                            <div class="kt-user-card__details">
                                <div class="kt-user-card__name">Luis</div>
                                <div class="kt-user-card__position">Santa.</div>
                            </div>
                        </div>
                    </div>
                    <ul class="kt-nav kt-margin-b-10">

                        <li class="kt-nav__separator kt-nav__separator--fit"></li>
                        <li class="kt-nav__custom kt-space-between">
                            <a href="#" class="btn btn-profile btn-upper btn-sm" data-toggle="modal" data-target="#logoutModal">Sign Out</a>
                            <i class="flaticon2-information kt-label-font-color-2" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Click to learn more..."></i>
                        </li>
                    </ul>
                </div>
            </div>
            <!--end: User Bar -->
        </div>

        <!-- end:: Header Topbar -->
                </div>