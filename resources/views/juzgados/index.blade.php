@extends('layouts.app')
@section('content')

    <div class="container-fluid">
        <div id="containerPrimary">
            <div class="row">
                <div class="col-md-4">

                </div>

                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header" style="text-align: center;">
                        Nuevo Juzgado
                        </div>
                        <div class="card-body" style="text-align: center;">
                        <button type="button" class="btn btn-info" onclick="NuevoJuzgado();">Nuevo (+)</button>
                        </div>
                    </div>

                </div>

                <div class="col-md-4">

                </div>

            </div>
        </div>

        <div id="containerSecondary " class="bg-white">
            <table class="table table-hover table-bordered table-sm">
                <thead>
                    <tr>
                        <th>Nombre Juzgado</th>
                        <th>Distrito</th>
                    </tr>
                </thead>
                <tbody>
                        @foreach ($juzgados as $item)
                    <tr>
                       
                         <td>{{$item->nombJuzgado}}</td>
                         <td>{{$item->Distrito->nombre}}</td>
                      
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $juzgados->links()!!}
        </div>

    </div>

@stop