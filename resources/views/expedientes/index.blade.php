@extends('layouts.app')
@section('content')

    <div class="container-fluid">
        <div id="containerPrimary">
            <div class="row">
                <div class="col-md-4">

                </div>

                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header" style="text-align: center;">
                        Nuevo Expediente
                        </div>
                        <div class="card-body" style="text-align: center;">
                        <button type="button" class="btn btn-info" onclick="NuevoExpediente();">Nuevo (+)</button>
                        </div>
                    </div>

                </div>

                <div class="col-md-4">

                </div>

            </div>
        </div>
        <form class="form-inline my-2 my-lg-0" action="{{ route('expedientes.index') }}">
            @csrf
            <input class="form-control mr-sm-3" type="search" name="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
        <div id="containerSecondary " class="bg-white">
            <table class="table table-hover table-bordered table-sm">
                <thead>
                    <tr>
                        <th>N° Expediente</th>
                        <th>Descripcion</th>
                        <th>Persona</th>
                        <th>Estado</th>
                        <th>Juzgado</th>
                        <th>Actividad</th>
                       
                    </tr>
                </thead>
                <tbody>
                        @foreach ($expedientes as $item)
                    <tr>
                       
                         
                         <td>{{$item->nro_exp}}</td>
                         <td>{{$item->descripcion}}</td>
                         <td>{{$item->persona->nombre}}</td>
                         <td>{{$item->estado->descripcion}}</td>
                         <td>{{$item->juzgado->nombJuzgado}}</td>
                         <td>{{ App\TipoActividad::find($item->actividad->idt_actividad)->descripcion}}</td>
                      
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $expedientes->links()!!}
        </div>

    </div>

@stop