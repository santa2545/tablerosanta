@extends('layouts.app')
@section('content')

    <div class="container-fluid">
        <div id="containerPrimary">
            <div class="row">
                <div class="col-md-4">

                </div>

                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header" style="text-align: center;">
                        Nuevo Calendario
                        </div>
                        <div class="card-body" style="text-align: center;">
                        <button type="button" class="btn btn-info" onclick="NuevoCalendario();">Nuevo (+)</button>
                        </div>
                    </div>

                </div>

                <div class="col-md-4">

                </div>

            </div>
        </div>

        <div id="containerSecondary " class="bg-white">
            <table class="table table-hover table-bordered table-sm">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Tipo</th>
                        <th>Descripcion</th>
                        <th>Asunto</th>
                        <th>Juzgado</th>
                        <th>Persona</th>
                        <th>Nota</th>
                        <th>Nº Expediente</th>
                    </tr>
                </thead>
                <tbody>
                        @foreach ($calendarios as $item)
                    <tr>
                       
                         <td>{{$item->id_calendario}}</td>
                         <td>{{$item->tipo}}</td>
                         <td>{{$item->descripcion}}</td>
                         <td>{{$item->asunto}}</td>
                         <td>{{$item->Juzgado->nombJuzgado}}</td>
                         <td>{{$item->Persona->nombre}}</td>
                         <td>{{$item->Nota->descripcion}}</td>
                         <td>{{$item->Expediente->nro_exp}}</td>
                      
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $calendarios->links()!!}
        </div>

    </div>

@stop