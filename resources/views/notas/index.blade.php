@extends('layouts.app')
@section('content')

    <div class="container-fluid">
        <div id="containerPrimary">
            <div class="row">
                <div class="col-md-4">

                </div>

                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header" style="text-align: center;">
                        Nueva Nota
                        </div>
                        <div class="card-body" style="text-align: center;">
                        <button type="button" class="btn btn-info" onclick="NuevaNota();">Nuevo (+)</button>
                        </div>
                    </div>

                </div>

                <div class="col-md-4">

                </div>

            </div>
        </div>

        <div id="containerSecondary " class="bg-white">
            <table class="table table-hover table-bordered table-sm">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nota</th>
                    </tr>
                </thead>
                <tbody>
                        @foreach ($notas as $item)
                    <tr>
                       
                         <td>{{$item->id_nota}}</td>
                         <td>{{$item->descripcion}}</td>
                      
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $notas->links()!!}
        </div>

    </div>

@stop