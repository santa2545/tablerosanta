@extends('layouts.app')
@section('content')

    <div class="container-fluid">
        <div id="containerPrimary">
            <div class="row">
                <div class="col-md-4">

                </div>

                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header" style="text-align: center;">
                        Nueva Tipo Actividad
                        </div>
                        <div class="card-body" style="text-align: center;">
                        <button type="button" class="btn btn-info" onclick="Nuevo_TipoActividad();">Nuevo (+)</button>
                        </div>
                    </div>

                </div>

                <div class="col-md-4">

                </div>

            </div>
        </div>

        <div id="containerSecondary " class="bg-white">
            <table class="table table-hover table-bordered table-sm">
                <thead>
                    <tr>
                        <th>Codigo</th>
                        <th>Nombre</th>
                       
                    </tr>
                </thead>
                <tbody>
                        @foreach ($tipoActividades as $item)
                    <tr>
                       
                         
                         <td>{{$item->idt_actividad}}</td>
                         <td>{{$item->descripcion}}</td>
                      
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $tipoActividades->links()!!}
        </div>

    </div>

@stop