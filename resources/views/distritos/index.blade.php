@extends('layouts.app')
@section('content')

    <div class="container-fluid">
        <div id="containerPrimary">
            <div class="row">
                <div class="col-md-4">

                </div>

                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header" style="text-align: center;">
                        Nuevo Distrito
                        </div>
                        <div class="card-body" style="text-align: center;">
                        <button type="button" class="btn btn-info" onclick="NuevoDistrito();">Nuevo (+)</button>
                        </div>
                    </div>

                </div>

                <div class="col-md-4">

                </div>

            </div>
        </div>

        <div id="containerSecondary " class="bg-white">
            <table class="table table-hover table-bordered table-sm">
                <thead>
                    <tr>
                        <th>Codigo</th>
                        <th>Distrito</th>
                    </tr>
                </thead>
                <tbody>
                        @foreach ($distritos as $item)
                    <tr>
                       
                         <td>{{$item->id_distrito}}</td>
                         <td>{{$item->nombre}}</td>
                      
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $distritos->links()!!}
        </div>

    </div>

@stop