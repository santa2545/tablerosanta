<?php 

 ?>

 <!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Inicio de Sesion</title>

    <!-- Bootstrap core CSS-->
    <link href="lib/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="lib/alertify/css/alertify.min.css" />

  </head>

  <body class="bg-dark">

    <div class="container">
      <div class="card card-login mx-auto mt-5" style="max-width: 25rem;">
        <!--<div class="card-header" style="text-align: center;">Ingreso</div>-->

        <div class="card-body">
         
        <!-- Default form login -->
            <form action="controlador/login.php" name="login" method="POST" id="form" class="text-center border border-light p-5">

                <p class="h4 mb-4">Ingresar</p>

                <!-- Email -->
                <input type="text" name="usuario" id="usuario" class="form-control mb-4" placeholder="Ingrese Usuario">

                <!-- Password -->
                <input type="password" name="password" id="password" class="form-control mb-4" placeholder="Ingrese Password">
            
                <div class="enter" id="logearse">
                    <!-- Sign in button -->
                    <input type="submit" id="logearse" name="submit" class="btn btn-info btn-block my-4" value="Ingresar" type="submit" />
                </div>
                



            </form>
            <!-- Default form login -->

        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="lib/jquery/dist/jquery.min.js"></script>
    <script src="lib/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="lib/alertify/alertify.min.js"></script>


    <script type="text/javascript">

    $(document).ready(function(){
       $("#usuario").focus();

    });


    $(document).on('submit', '#form', function(event) {
        event.preventDefault();

        $.ajax({
              type:'POST',
              url: 'controlador/login.php',
              dataType: 'json',
              data: $(this).serialize(),
              beforeSend: function(){

              }
        })
        .done(function(respuesta) {
          console.log(respuesta);
          if(!respuesta.error){
            location.href = 'View/Dashboard.php';
          }

          else {
            alertify.error('Usuario y/o contraseña no coinciden...');

          }

        })
        .fail(function(resp) {
          console.log(resp.responseText);
        })
        .always(function() {
          //console.log("complete");
        });
    
    });

   
    
</script>

  </body>

</html>
