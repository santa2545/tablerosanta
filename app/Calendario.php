<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calendario extends Model
{
	 protected $primaryKey = 'id_calendario';

	 protected $fillable = ['tipo','descripcion','asunto','id_juzgado','id_persona','id_nota','id_expediente'];

	 public function juzgado(){

		return $this->belongsTo('App\Juzgado', 'id_juzgado', 'id_juzgado');
   	}
	 
	 public function persona(){

          return $this->belongsTo('App\Persona', 'id_persona', 'id_persona');
     }

     public function nota(){

          return $this->belongsTo('App\Nota', 'id_nota', 'id_nota');
     }

     public function Expediente(){

          return $this->belongsTo('App\Expediente', 'id_expediente', 'id_expediente');
     }
}
