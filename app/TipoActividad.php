<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoActividad extends Model
{

      protected $primaryKey = 'idt_actividad';

      protected $fillable = ['descripcion'];
}
