<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Distrito extends Model
{
     protected $primaryKey = 'id_distrito';
     protected $fillable = ['nombre'];
}
