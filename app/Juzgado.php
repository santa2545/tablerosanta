<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Juzgado extends Model
{
     protected $primaryKey = 'id_juzgado';

     protected $fillable = ['nombJuzgado','id_distrito'];

     public function distrito(){

          return $this->belongsTo('App\Distrito', 'id_distrito', 'id_distrito');
     }
}
