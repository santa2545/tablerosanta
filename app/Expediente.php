<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expediente extends Model
{
     protected $primaryKey = 'id_expediente';
     protected $fillable = ['nro_exp','descripcion','id_persona','id_proceso','id_estado','id_juzgado','id_actividad'];

     public function persona(){

          return $this->belongsTo('App\Persona', 'id_persona', 'id_persona');
     }

     public function estado(){

          return $this->belongsTo('App\Estado', 'id_estado', 'id_estado');
     }

     public function juzgado(){

          return $this->belongsTo('App\Juzgado', 'id_juzgado', 'id_juzgado');
     }
     public function actividad(){

          return $this->belongsTo('App\Actividad', 'id_actividad', 'id_actividad');
     }

     public function TipoActividad(){

          return $this->belongsTo('App\TipoActividad', 'idt_actividad', 'id_actividad');
     }

}
