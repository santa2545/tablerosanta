const HTTP_GET = "GET";
const HTTP_POST = "POST";
const HTTP_PUT = "PUT";
const HTTP_DELETE = "DELETE";

const CONTAINER_PRIMARY = "containerPrimary";
const CONTAINER_SECONDARY = "containerSecondary";